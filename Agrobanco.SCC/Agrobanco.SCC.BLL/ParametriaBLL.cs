﻿using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.BLL
{
    public class ParametriaBLL
    {
        private string strCadenaConexion;
        public ParametriaBLL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
        }

        public List<Parametro> ObtenerListaEmpresa()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> empresas = parametria.ObtenerListaEmpresa();

            return empresas;
        }

        public List<Parametro> ObtenerListaArea()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> areas = parametria.ObtenerListaArea();

            return areas;
        }
        public List<Parametro> ObtenerListaFrecuencia()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> parametros = parametria.ObtenerListaParametro(1);

            return parametros;
        }

        public List<Parametro> ObtenerListaMedioEnvio()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> parametros = parametria.ObtenerListaParametro(2);

            return parametros;
        }

        public List<Parametro> ObtenerListaCorreosCumplimiento()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> parametros = parametria.ObtenerListaParametro(4);

            return parametros;
        }

        public List<Usuario> ObtenerListaUsuario()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Usuario> usuarios = parametria.ObtenerListaUsuario();

            return usuarios;
        }

        public List<Parametro> ObtenerListaMotivo()
        {
            Parametria parametria = new Parametria(strCadenaConexion);

            List<Parametro> motivos = parametria.ObtenerListaParametro(3);

            return motivos;
        }

    }
}
