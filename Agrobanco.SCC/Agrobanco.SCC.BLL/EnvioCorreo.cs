﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.BLL
{
    public class EnvioCorreo
    {
        public int EnviarCorreo(List<String> para, List<String> copia, string cuerpo)
        {
            int rpta = -1;
            MailMessage message = new MailMessage();
            try
            {
                var body = cuerpo;
                foreach (var item in para)
                {
                    message.To.Add(new MailAddress(item));
                }
                if (copia != null)
                {
                    foreach (var item in copia)
                    {
                        message.CC.Add(new MailAddress(item));
                    }
                }

                message.Subject = "Sistema de Control de Cumplimiento";
                message.Body = body;
                message.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {                    
                    smtp.Send(message);
                    rpta = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //GenerarLog objLOG = new GenerarLog();
                //string detalleError = "Envio Correo" + " - " +
                //    DateTime.Now + "\n" + "\n" +
                //    e.Message + "\n" +
                //    "-------------------------------------------------------------------------------------------------------------";
                //objLOG.GenerarArchivoLogApi(detalleError);
                //return rpta = -1;
            }
            return rpta;
        }
    }
}
