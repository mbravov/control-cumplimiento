﻿using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.BLL
{
    public class SeguimientoBLL
    {
        private string strCadenaConexion;
        public SeguimientoBLL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
        }

        public List<SeguimientoConsulta> ObtenerListaSeguimiento(string listaArea, DateTime? vencimientoDesde, DateTime? vencimientoHasta, int? estadoCumplimiento)
        {
            List<SeguimientoConsulta> listaSeguimiento = new List<SeguimientoConsulta>();

            Seguimiento seguimiento = new Seguimiento(strCadenaConexion);

            listaSeguimiento = seguimiento.ObtenerListaSeguimiento(listaArea, vencimientoDesde, vencimientoHasta, estadoCumplimiento);

            return listaSeguimiento;
        }    

    }
}
