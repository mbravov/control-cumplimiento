﻿using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.BLL
{
    
    public class CumplimientoBLL
    {
        private string strCadenaConexion;
        public CumplimientoBLL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
        }

        public List<Envio> ObtenerListaEnvioBusqueda(string listaEntidad, string listaFrecuencia, DateTime? vencimientoDesde, DateTime? vencimientoHasta)
        {
            Cumplimiento cumplimiento = new Cumplimiento(strCadenaConexion);

            List<Envio> envios = cumplimiento.ObtenerListaEnvioBusqueda(listaEntidad, listaFrecuencia, vencimientoDesde, vencimientoHasta);

            return envios;
        }

        public List<Envio> ObtenerListaEnvio(int? codEnvio = null)
        {
            Cumplimiento cumplimiento = new Cumplimiento(strCadenaConexion);

            List<Envio> envios = cumplimiento.ObtenerListaEnvio(codEnvio);

            return envios;
        }

        public DateTime RegistrarEnvio(Envio envio, Stream streamSustento)
        {
            Cumplimiento cumplimiento = new Cumplimiento(strCadenaConexion);
            Configuracion configuracion = new Configuracion(strCadenaConexion);
            ParametriaBLL parametria = new ParametriaBLL();

            DateTime resultado = new DateTime();            

            string nombreArchivo = ObtenerNombreArchivoSustento(envio.NombreReporte, envio.FechaVencimiento.Value);
            string fullNombreArchivo = string.Empty;

            int codSustento = CargarSustento(streamSustento, nombreArchivo, out fullNombreArchivo);
            envio.CodSustento = codSustento;
            envio.NombreArchivo = fullNombreArchivo;

            int resRegistro = cumplimiento.RegistrarEnvio(envio);

            if (envio.CodTipoCalculo == Convert.ToInt32(Enums.TipoCalculo.Automatico) && !(envio.CodFrecuencia == Convert.ToInt32(Enums.Frecuencia.ADemanda)))
            {
                //OBTENER NUEVA FECHA
                DateTime nuevaFechaVencimiento = cumplimiento.ObtenerNuevaFechaVencimiento(envio.CodReporte);
                resultado = nuevaFechaVencimiento;
                //GUARDAR NUEVA FECHA
                cumplimiento.ActualizarFechaVencimiento(envio.CodReporte, nuevaFechaVencimiento, envio.UsuarioCreacion);
            }

            if (envio.CodTipoCalculo == Convert.ToInt32(Enums.TipoCalculo.Manual) || (envio.CodFrecuencia == Convert.ToInt32(Enums.Frecuencia.ADemanda)))
            {
                
                List<Reporte> reportes = configuracion.ObtenerListaReporteVencimiento(envio.CodReporte);
                configuracion.ActualizarReporteNotificacion(envio.CodReporte, (int)Enums.TipoNotificacion.Vencimiento);
                configuracion.ActualizarReporteNotificacion(envio.CodReporte, (int)Enums.TipoNotificacion.Incumplimiento);

                //ENVIAR CORREO AL ÁREA DE CUMPLIMIENTO
                var reporte = reportes.FirstOrDefault();
                string ruta = ConfigurationManager.AppSettings["rutaPlantillas"] + "ActualizacionVencimiento.html";

                string body = GenerarCuerpoCorreo(ruta, reporte);

                var usersCumplimiento = parametria.ObtenerListaCorreosCumplimiento();
                List<string> emailCumplimiento = usersCumplimiento.Select(u => u.Descripcion).ToList();

                List<string> listaPara = new List<string>();
                List<string> listaCopia = new List<string>();

                listaPara.Add(reporte.EmailResponsablePrincipal);
                listaPara.Add(reporte.EmailResponsableAlterno);
                listaCopia.Add(reporte.EmailRespGeneracion1);
                listaCopia.AddRange(emailCumplimiento);

                EnvioCorreo envioCorreo = new EnvioCorreo();

                envioCorreo.EnviarCorreo(listaPara, listaCopia, body);

                resultado = envio.FechaVencimiento.Value;
            }

            return resultado;
        }

        private int CargarSustento(Stream streamSustento, string nombreArchivo, out string fullNombreArchivo)
        {
            Laserfiche laserfiche = new Laserfiche();
            int codSustento = 0;            
            codSustento = laserfiche.CargarDocumentoLaserFiche(streamSustento, "pdf", nombreArchivo, out fullNombreArchivo);

            return codSustento;
        }

        private string ObtenerNombreArchivoSustento(string nombreReporte, DateTime fechaVencimiento)
        {
            string nombreArchivo = nombreReporte;
            string concatNomReporte = string.Empty;

            if (nombreReporte.Contains(" "))
            {
                string[] arrNombreReporte = nombreReporte.Split(' ');

                for (int i = 0; i < arrNombreReporte.Length; i++)
                {
                    string strItem = arrNombreReporte[i];

                    if (strItem.Trim() != "")
                    {
                        concatNomReporte += "_" + strItem;
                    }
                }

                concatNomReporte = concatNomReporte.Remove(0, 1);
            }

            if (!nombreReporte.Contains(" "))
            {
                concatNomReporte = nombreReporte;
            }

            string dd = fechaVencimiento.Day.ToString().PadLeft(2, '0');
            string mm = fechaVencimiento.Month.ToString().PadLeft(2, '0');
            string yyyy = fechaVencimiento.Year.ToString();

            nombreArchivo = string.Concat(concatNomReporte, "_", dd, mm, yyyy);

            return nombreArchivo;
        }

        public string ObtenerSustento(int codigoSustento, out string fileName, out string extension)
        {
            Laserfiche laserfiche = new Laserfiche();
            string fileBase64 = laserfiche.ConsultaArchivoSimple(codigoSustento, out fileName, out extension);

            return fileBase64;
        }

        private string GenerarCuerpoCorreo(string ruta, Reporte reporte)
        {
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(ruta))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{NombreReporte}", reporte.NombreReporte);
            body = body.Replace("{DescripcionReporte}", reporte.Descripcion);
            body = body.Replace("{Entidad}", reporte.Empresa);

            string fecha = reporte.ProximoVencimiento.Day.ToString().PadLeft(2, '0') + "/" + reporte.ProximoVencimiento.Month.ToString().PadLeft(2, '0') + "/" + reporte.ProximoVencimiento.Year.ToString();
            body = body.Replace("{FechaVencimiento}", fecha);

            return body;
        }

    }
}
