﻿using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.BLL
{
    public class ConfiguracionBLL
    {
        private string strCadenaConexion;
        public ConfiguracionBLL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
        }

        public List<Reporte> ObtenerListaReporte(int? idReporte = null)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);

            List<Reporte> reportes = configuracion.ObtenerListaReporte(idReporte);

            return reportes;
        }

        public List<Reporte> ObtenerListaReporteVencimiento(int? idReporte = null)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);

            List<Reporte> reportes = configuracion.ObtenerListaReporteVencimiento(idReporte);

            return reportes;
        }


        public List<Reporte> ObtenerListaReporteBusqueda(string listaArea, DateTime? vencimientoDesde, DateTime? vencimientoHasta)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);

            List<Reporte> reportes = configuracion.ObtenerListaReporteBusqueda(listaArea, vencimientoDesde, vencimientoHasta);

            return reportes;
        }

        public int RegistrarReporte(Reporte reporte)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);
            int resultado = 0;

            resultado = configuracion.RegistrarReporte(reporte);

            return resultado;
        }

        public bool ValidarNombreReporte(int codigoReporte, string nombreReporte)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);
            bool resultadoValidacion = true;

            var reportes = configuracion.ValidarNombreReporte(nombreReporte);

            if (reportes.Any(r=> r.CodigoReporte != codigoReporte))
            {
                resultadoValidacion = false;
            }

            return resultadoValidacion;
        }
        
        public int ActualizarReporteNotificacion(int codigoReporte, int tipoNotificacion)
        {
            Configuracion configuracion = new Configuracion(strCadenaConexion);
            int resultado = 0;

            resultado = configuracion.ActualizarReporteNotificacion(codigoReporte, tipoNotificacion);

            return resultado;
        }

    }
}
