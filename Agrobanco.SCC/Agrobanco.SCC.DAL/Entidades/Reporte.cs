﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class Reporte
    {
        public int CodigoReporte { get; set; }
        public string NombreReporte { get; set; }
        public string Descripcion { get; set; }
        public int CodigoEmpresa { get; set; }
        public string Empresa { get; set; }
        public int? MedioEnvio { get; set; }
        public string MedioEnvioDescripcion { get; set; }
        public int CodAreaGeneracion { get; set; }
        public string AreaGeneracion { get; set; }
        public int CodGerenteGeneracion { get; set; }
        public string GerenteGeneracion { get; set; }
        public int CodResponsableGen1 { get; set; }
        public string ResponsableGeneracion1 { get; set; }
        public string EmailRespGeneracion1 { get; set; }
        public int CodResponsableGen2 { get; set; }
        public string ResponsableGeneracion2 { get; set; }
        public int CodAreaResponsable { get; set; }
        public string AreaResponsable { get; set; }
        public int CodGerenteResponsable { get; set; }
        public string GerenteResponsable { get; set; }
        public string EmailGerenteResponsable { get; set; }
        public int CodCoordinador { get; set; }
        public string Coordinador { get; set; }
        public string EmailCoordinador { get; set; }
        public int CodResponsablePrincipal { get; set; }
        public string ResponsablePrincipal { get; set; }
        public string EmailResponsablePrincipal { get; set; }
        public int? CodResponsableAlterno { get; set; }
        public string ResponsableAlterno { get; set; }
        public string EmailResponsableAlterno { get; set; }

        public int CodFrecuencia { get; set; }
        public string Frecuencia { get; set; }
        public DateTime ProximoVencimiento { get; set; }
        public int TipoCalculo { get; set; }
        public int Plazo { get; set; }
        public int TipoDiasPlazo { get; set; }
        public string TipoDiasPlazoDescripcion { get; set; }
        public int DiasRecordatorio1 { get; set; }
        public DateTime FechaRecordatorio1 { get; set; }
        public int DiasRecordatorio2{ get; set; }
        public DateTime FechaRecordatorio2 { get; set; }
        public int Estado { get; set; }
        public string UsuCreacion { get; set; }
        public string UsuActualizacion { get; set; }
        public bool Recordatorio1 { get; set; }
        public bool Recordatorio2 { get; set; }
        public bool AlertaVencimiento { get; set; }
        public bool AlertaIncumplimiento { get; set; }
    }
}
