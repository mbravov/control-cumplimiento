﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class SeguimientoConsulta : Reporte
    {
        public int Cumplimiento { get; set; }
        public string DescripcionCumplimiento { get; set; }
        public string NombreReporteE { get; set; }
        public string ResponsablePrincipalE { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public int MesVencimiento { get; set; }
        public int AnioVencimiento { get; set; }
        public DateTime? FechaEnvio { get; set; }
        public string MotivoIncumplimiento { get; set; }
        public string Comentarios { get; set; }
    }
}
