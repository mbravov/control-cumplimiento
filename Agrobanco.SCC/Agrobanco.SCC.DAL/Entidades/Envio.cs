﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class Envio
    {
        public int CodEnvio { get; set; }
        public int CodReporte { get; set; }
        public string NombreReporte { get; set; }
        public int CodEntidad { get; set; }
        public string Entidad { get; set; }
        public int CodAreaResponsable { get; set; }
        public string AreaResponsable { get; set; }
        public int CodCoordinador { get; set; }
        public int CodGerenteResponsable { get; set; }
        public string GerenteResponsable { get; set; }
        public int CodResponsablePrincipal { get; set; }
        public string ResponsablePrincipal { get; set; }
        public int CodResponsableAlterno { get; set; }
        public string ResponsableAlterno { get; set; }
        public int CodEstadoEnvio { get; set; }
        public string EstadoEnvio { get; set; }
        public int CodVencimiento { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public DateTime? FechaEnvio { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string MotivoIncumplimiento { get; set; }
        public string Comentarios { get; set; }
        public int CodSustento { get; set; }
        public string NombreArchivo { get; set; }
        public int CodTipoCalculo { get; set; }
        public int CodFrecuencia { get; set; }
        public string Frecuencia { get; set; }

        public string UsuarioCreacion { get; set; }
    }
}
