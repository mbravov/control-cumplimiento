﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class Parametro
    {
        public int CodigoParametro { get; set; }
        public int CodigoGrupo { get; set; }
        public int CodigoElemento { get; set; }
                
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public int Orden { get; set; }
        public int Estado { get; set; }
        
    }
}
