﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class Usuario
    {
        public int CodUsuario { get; set; }
        public string Nombres { get; set; }
        public string Email { get; set; }
        public string WebUser { get; set; }
        public string Cargo { get; set; }
        public int CodArea { get; set; }
        public int Estado { get; set; }
    }
}
