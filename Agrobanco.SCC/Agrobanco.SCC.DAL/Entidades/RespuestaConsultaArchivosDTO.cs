﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL.Entidades
{
    public class RespuestaConsultaArchivosDTO : RespuestaBaseDTO
    {
        public List<ConsultaArchivoDTO> ListaDocumentos { get; set; }
    }
}
