﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL
{
    public static class Enums
    {
        public enum TipoCalculo
        {
            Automatico = 1,
            Manual = 2
        }

        public enum EstadoEnvio
        {
            Enviado = 1,
            NoEnviado = 0
        }

        public enum Frecuencia
        {
            Diario = 1,
            Quincenal = 2,
            Mensual = 3,
            Trimestral = 4,
            Cuatrimestral = 5,
            Semestral = 6,
            Anual = 7,
            ADemanda = 8
        }

        public enum TipoNotificacion
        {
            Recordatorio1 = 1,
            Recordatorio2 = 2,
            Vencimiento = 3,
            Incumplimiento = 4
        }
    }
}
