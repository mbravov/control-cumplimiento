﻿using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL
{
    public class Seguimiento
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        public Seguimiento(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        public List<SeguimientoConsulta> ObtenerListaSeguimiento(string listaArea, DateTime? vencimientoDesde, DateTime? vencimientoHasta, int? estadoCumplimiento)
        {
            try
            {
                List<SeguimientoConsulta> reportes = new List<SeguimientoConsulta>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaSeguimiento]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@Areas", SqlDbType.VarChar, 50);
                        param1.Value = listaArea;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@VctoDesde", SqlDbType.DateTime);
                        if (vencimientoDesde.HasValue)
                        {
                            param1.Value = vencimientoDesde.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }

                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@VctoHasta", SqlDbType.DateTime);

                        if (vencimientoHasta.HasValue)
                        {
                            param1.Value = vencimientoHasta.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }

                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@EstadoCumplimiento", SqlDbType.Int);
                        param1.Value = (object)estadoCumplimiento ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SeguimientoConsulta reporte = new SeguimientoConsulta();

                                if (!Convert.IsDBNull(reader["iCodReporte"]))
                                {
                                    reporte.CodigoReporte = Convert.ToInt32(reader["iCodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombre"]))
                                {
                                    reporte.NombreReporte = Convert.ToString(reader["vNombre"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    reporte.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodEmpresa"]))
                                {
                                    reporte.CodigoEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["Empresa"]))
                                {
                                    reporte.Empresa = Convert.ToString(reader["Empresa"]);
                                }

                                if (!Convert.IsDBNull(reader["iMedioEnvio"]))
                                {
                                    reporte.MedioEnvio = Convert.ToInt32(reader["iMedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["MedioEnvio"]))
                                {
                                    reporte.MedioEnvioDescripcion = Convert.ToString(reader["MedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaGen"]))
                                {
                                    reporte.CodAreaGeneracion = Convert.ToInt32(reader["iCodAreaGen"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaGeneracion"]))
                                {
                                    reporte.AreaGeneracion = Convert.ToString(reader["AreaGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteGen"]))
                                {
                                    reporte.CodGerenteGeneracion = Convert.ToInt32(reader["iCodGerenteGen"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteGeneracion"]))
                                {
                                    reporte.GerenteGeneracion = Convert.ToString(reader["GerenteGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen1"]))
                                {
                                    reporte.CodResponsableGen1 = Convert.ToInt32(reader["iCodResponsableGen1"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion1"]))
                                {
                                    reporte.ResponsableGeneracion1 = Convert.ToString(reader["ResponsableGeneracion1"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen2"]))
                                {
                                    reporte.CodResponsableGen2 = Convert.ToInt32(reader["iCodResponsableGen2"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion2"]))
                                {
                                    reporte.ResponsableGeneracion2 = Convert.ToString(reader["ResponsableGeneracion2"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaResponsable"]))
                                {
                                    reporte.CodAreaResponsable = Convert.ToInt32(reader["iCodAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaResponsable"]))
                                {
                                    reporte.AreaResponsable = Convert.ToString(reader["AreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteResponsable"]))
                                {
                                    reporte.CodGerenteResponsable = Convert.ToInt32(reader["iCodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteResponsable"]))
                                {
                                    reporte.GerenteResponsable = Convert.ToString(reader["GerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodCoordinador"]))
                                {
                                    reporte.CodCoordinador = Convert.ToInt32(reader["iCodCoordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["Coordinador"]))
                                {
                                    reporte.Coordinador = Convert.ToString(reader["Coordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsablePrincipal"]))
                                {
                                    reporte.CodResponsablePrincipal = Convert.ToInt32(reader["iCodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsablePrincipal"]))
                                {
                                    reporte.ResponsablePrincipal = Convert.ToString(reader["ResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableAlterno"]))
                                {
                                    reporte.CodResponsableAlterno = Convert.ToInt32(reader["iCodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableAlterno"]))
                                {
                                    reporte.ResponsableAlterno = Convert.ToString(reader["ResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodFrecuencia"]))
                                {
                                    reporte.CodFrecuencia = Convert.ToInt32(reader["iCodFrecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    reporte.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["dProximoVencimiento"]))
                                {
                                    reporte.ProximoVencimiento = Convert.ToDateTime(reader["dProximoVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodTipoCalculo"]))
                                {
                                    reporte.TipoCalculo = Convert.ToInt32(reader["iCodTipoCalculo"]);
                                }

                                if (!Convert.IsDBNull(reader["iPlazo"]))
                                {
                                    reporte.Plazo = Convert.ToInt32(reader["iPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iTipoDiasPlazo"]))
                                {
                                    reporte.TipoDiasPlazo = Convert.ToInt32(reader["iTipoDiasPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio1"]))
                                {
                                    reporte.DiasRecordatorio1 = Convert.ToInt32(reader["iDiasRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio2"]))
                                {
                                    reporte.DiasRecordatorio2 = Convert.ToInt32(reader["iDiasRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    reporte.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                if (!Convert.IsDBNull(reader["Cumplimiento"]))
                                {
                                    reporte.Cumplimiento = Convert.ToInt32(reader["Cumplimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["DescripcionCumplimiento"]))
                                {
                                    reporte.DescripcionCumplimiento = Convert.ToString(reader["DescripcionCumplimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["MesVencimiento"]))
                                {
                                    reporte.MesVencimiento = Convert.ToInt32(reader["MesVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["AnioVencimiento"]))
                                {
                                    reporte.AnioVencimiento = Convert.ToInt32(reader["AnioVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodEnvio"]))
                                {
                                    if (!Convert.IsDBNull(reader["NombreReporteE"]))
                                    {
                                        reporte.NombreReporteE = Convert.ToString(reader["NombreReporteE"]);
                                    }

                                    if (!Convert.IsDBNull(reader["ResponsablePrincipalE"]))
                                    {
                                        reporte.ResponsablePrincipalE = Convert.ToString(reader["ResponsablePrincipalE"]);
                                    }

                                    if (!Convert.IsDBNull(reader["FechaRegistro"]))
                                    {
                                        reporte.FechaRegistro = Convert.ToDateTime(reader["FechaRegistro"]);
                                    }

                                    if (!Convert.IsDBNull(reader["FechaVencimiento"]))
                                    {
                                        reporte.FechaVencimiento = Convert.ToDateTime(reader["FechaVencimiento"]);
                                    }

                                    if (!Convert.IsDBNull(reader["FechaEnvio"]))
                                    {
                                        reporte.FechaEnvio = Convert.ToDateTime(reader["FechaEnvio"]);
                                    }

                                    if (!Convert.IsDBNull(reader["vMotivoIncumplimiento"]))
                                    {
                                        reporte.MotivoIncumplimiento = Convert.ToString(reader["vMotivoIncumplimiento"]);
                                    }

                                    if (!Convert.IsDBNull(reader["vComentarios"]))
                                    {
                                        reporte.Comentarios = Convert.ToString(reader["vComentarios"]);
                                    }
                                }

                                reportes.Add(reporte);
                            }

                            return reportes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
