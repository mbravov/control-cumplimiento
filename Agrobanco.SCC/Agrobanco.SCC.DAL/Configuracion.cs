﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Agrobanco.SCC.DAL.Entidades;
using System.Data;

namespace Agrobanco.SCC.DAL
{
    public class Configuracion
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        public Configuracion(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        public List<Reporte> ObtenerListaReporte(int? idReporte = null)
        {
            try
            {
                List<Reporte> reportes = new List<Reporte>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaReporte]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = (object)idReporte ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Reporte reporte = new Reporte();

                                if (!Convert.IsDBNull(reader["iCodReporte"]))
                                {
                                    reporte.CodigoReporte = Convert.ToInt32(reader["iCodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombre"]))
                                {
                                    reporte.NombreReporte = Convert.ToString(reader["vNombre"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    reporte.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodEmpresa"]))
                                {
                                    reporte.CodigoEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["Empresa"]))
                                {
                                    reporte.Empresa = Convert.ToString(reader["Empresa"]);
                                }

                                if (!Convert.IsDBNull(reader["iMedioEnvio"]))
                                {
                                    reporte.MedioEnvio = Convert.ToInt32(reader["iMedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["MedioEnvio"]))
                                {
                                    reporte.MedioEnvioDescripcion = Convert.ToString(reader["MedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaGen"]))
                                {
                                    reporte.CodAreaGeneracion = Convert.ToInt32(reader["iCodAreaGen"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaGeneracion"]))
                                {
                                    reporte.AreaGeneracion = Convert.ToString(reader["AreaGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteGen"]))
                                {
                                    reporte.CodGerenteGeneracion = Convert.ToInt32(reader["iCodGerenteGen"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteGeneracion"]))
                                {
                                    reporte.GerenteGeneracion = Convert.ToString(reader["GerenteGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen1"]))
                                {
                                    reporte.CodResponsableGen1 = Convert.ToInt32(reader["iCodResponsableGen1"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion1"]))
                                {
                                    reporte.ResponsableGeneracion1 = Convert.ToString(reader["ResponsableGeneracion1"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen2"]))
                                {
                                    reporte.CodResponsableGen2 = Convert.ToInt32(reader["iCodResponsableGen2"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion2"]))
                                {
                                    reporte.ResponsableGeneracion2 = Convert.ToString(reader["ResponsableGeneracion2"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaResponsable"]))
                                {
                                    reporte.CodAreaResponsable = Convert.ToInt32(reader["iCodAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaResponsable"]))
                                {
                                    reporte.AreaResponsable = Convert.ToString(reader["AreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteResponsable"]))
                                {
                                    reporte.CodGerenteResponsable = Convert.ToInt32(reader["iCodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteResponsable"]))
                                {
                                    reporte.GerenteResponsable = Convert.ToString(reader["GerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodCoordinador"]))
                                {
                                    reporte.CodCoordinador = Convert.ToInt32(reader["iCodCoordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["Coordinador"]))
                                {
                                    reporte.Coordinador = Convert.ToString(reader["Coordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsablePrincipal"]))
                                {
                                    reporte.CodResponsablePrincipal = Convert.ToInt32(reader["iCodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsablePrincipal"]))
                                {
                                    reporte.ResponsablePrincipal = Convert.ToString(reader["ResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableAlterno"]))
                                {
                                    reporte.CodResponsableAlterno = Convert.ToInt32(reader["iCodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableAlterno"]))
                                {
                                    reporte.ResponsableAlterno = Convert.ToString(reader["ResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodFrecuencia"]))
                                {
                                    reporte.CodFrecuencia = Convert.ToInt32(reader["iCodFrecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    reporte.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["dProximoVencimiento"]))
                                {
                                    reporte.ProximoVencimiento = Convert.ToDateTime(reader["dProximoVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodTipoCalculo"]))
                                {
                                    reporte.TipoCalculo = Convert.ToInt32(reader["iCodTipoCalculo"]);
                                }

                                if (!Convert.IsDBNull(reader["iPlazo"]))
                                {
                                    reporte.Plazo = Convert.ToInt32(reader["iPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iTipoDiasPlazo"]))
                                {
                                    reporte.TipoDiasPlazo = Convert.ToInt32(reader["iTipoDiasPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio1"]))
                                {
                                    reporte.DiasRecordatorio1 = Convert.ToInt32(reader["iDiasRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio2"]))
                                {
                                    reporte.DiasRecordatorio2 = Convert.ToInt32(reader["iDiasRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    reporte.Estado = Convert.ToInt32(reader["iEstado"]);
                                }
                                
                                reportes.Add(reporte);
                            }

                            return reportes;
                       }                       
                    }
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public List<Reporte> ObtenerListaReporteVencimiento(int? idReporte = null)
        {
            try
            {
                List<Reporte> reportes = new List<Reporte>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaReporteVencimiento]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = (object)idReporte ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Reporte reporte = new Reporte();

                                if (!Convert.IsDBNull(reader["iCodReporte"]))
                                {
                                    reporte.CodigoReporte = Convert.ToInt32(reader["iCodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombre"]))
                                {
                                    reporte.NombreReporte = Convert.ToString(reader["vNombre"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    reporte.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodEmpresa"]))
                                {
                                    reporte.CodigoEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["Empresa"]))
                                {
                                    reporte.Empresa = Convert.ToString(reader["Empresa"]);
                                }

                                if (!Convert.IsDBNull(reader["iMedioEnvio"]))
                                {
                                    reporte.MedioEnvio = Convert.ToInt32(reader["iMedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["MedioEnvio"]))
                                {
                                    reporte.MedioEnvioDescripcion = Convert.ToString(reader["MedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaGen"]))
                                {
                                    reporte.CodAreaGeneracion = Convert.ToInt32(reader["iCodAreaGen"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaGeneracion"]))
                                {
                                    reporte.AreaGeneracion = Convert.ToString(reader["AreaGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteGen"]))
                                {
                                    reporte.CodGerenteGeneracion = Convert.ToInt32(reader["iCodGerenteGen"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteGeneracion"]))
                                {
                                    reporte.GerenteGeneracion = Convert.ToString(reader["GerenteGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen1"]))
                                {
                                    reporte.CodResponsableGen1 = Convert.ToInt32(reader["iCodResponsableGen1"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion1"]))
                                {
                                    reporte.ResponsableGeneracion1 = Convert.ToString(reader["ResponsableGeneracion1"]);
                                }

                                if (!Convert.IsDBNull(reader["EmailRespGeneracion1"]))
                                {
                                    reporte.EmailRespGeneracion1 = Convert.ToString(reader["EmailRespGeneracion1"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen2"]))
                                {
                                    reporte.CodResponsableGen2 = Convert.ToInt32(reader["iCodResponsableGen2"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion2"]))
                                {
                                    reporte.ResponsableGeneracion2 = Convert.ToString(reader["ResponsableGeneracion2"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaResponsable"]))
                                {
                                    reporte.CodAreaResponsable = Convert.ToInt32(reader["iCodAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaResponsable"]))
                                {
                                    reporte.AreaResponsable = Convert.ToString(reader["AreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteResponsable"]))
                                {
                                    reporte.CodGerenteResponsable = Convert.ToInt32(reader["iCodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteResponsable"]))
                                {
                                    reporte.GerenteResponsable = Convert.ToString(reader["GerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["EmailGerenteResponsable"]))
                                {
                                    reporte.EmailGerenteResponsable = Convert.ToString(reader["EmailGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodCoordinador"]))
                                {
                                    reporte.CodCoordinador = Convert.ToInt32(reader["iCodCoordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["Coordinador"]))
                                {
                                    reporte.Coordinador = Convert.ToString(reader["Coordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["EmailCoordinador"]))
                                {
                                    reporte.EmailCoordinador = Convert.ToString(reader["EmailCoordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsablePrincipal"]))
                                {
                                    reporte.CodResponsablePrincipal = Convert.ToInt32(reader["iCodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsablePrincipal"]))
                                {
                                    reporte.ResponsablePrincipal = Convert.ToString(reader["ResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["EmailRespPrincipal"]))
                                {
                                    reporte.EmailResponsablePrincipal = Convert.ToString(reader["EmailRespPrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableAlterno"]))
                                {
                                    reporte.CodResponsableAlterno = Convert.ToInt32(reader["iCodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableAlterno"]))
                                {
                                    reporte.ResponsableAlterno = Convert.ToString(reader["ResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["EmailRespAlterno"]))
                                {
                                    reporte.EmailResponsableAlterno = Convert.ToString(reader["EmailRespAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodFrecuencia"]))
                                {
                                    reporte.CodFrecuencia = Convert.ToInt32(reader["iCodFrecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    reporte.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["dProximoVencimiento"]))
                                {
                                    reporte.ProximoVencimiento = Convert.ToDateTime(reader["dProximoVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodTipoCalculo"]))
                                {
                                    reporte.TipoCalculo = Convert.ToInt32(reader["iCodTipoCalculo"]);
                                }

                                if (!Convert.IsDBNull(reader["iPlazo"]))
                                {
                                    reporte.Plazo = Convert.ToInt32(reader["iPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iTipoDiasPlazo"]))
                                {
                                    reporte.TipoDiasPlazo = Convert.ToInt32(reader["iTipoDiasPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio1"]))
                                {
                                    reporte.DiasRecordatorio1 = Convert.ToInt32(reader["iDiasRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["FechaRecordatorio1"]))
                                {
                                    reporte.FechaRecordatorio1 = Convert.ToDateTime(reader["FechaRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio2"]))
                                {
                                    reporte.DiasRecordatorio2 = Convert.ToInt32(reader["iDiasRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["FechaRecordatorio2"]))
                                {
                                    reporte.FechaRecordatorio2 = Convert.ToDateTime(reader["FechaRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    reporte.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                if (!Convert.IsDBNull(reader["iRecordatorio1"]))
                                {
                                    reporte.Recordatorio1 = Convert.ToBoolean(reader["iRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["iRecordatorio2"]))
                                {
                                    reporte.Recordatorio2 = Convert.ToBoolean(reader["iRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["iAlertaVencimiento"]))
                                {
                                    reporte.AlertaVencimiento = Convert.ToBoolean(reader["iAlertaVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iAlertaIncumplimiento"]))
                                {
                                    reporte.AlertaIncumplimiento = Convert.ToBoolean(reader["iAlertaIncumplimiento"]);
                                }

                                reportes.Add(reporte);
                            }

                            return reportes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RegistrarReporte(Reporte reporte)
        {
            try
            {
                int resultado = 0;

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_RegistrarReporte]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = (object)reporte.CodigoReporte ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Nombre", SqlDbType.VarChar, 50);
                        param1.Value = (object)reporte.NombreReporte ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Descripcion", SqlDbType.VarChar, 300);
                        param1.Value = reporte.Descripcion;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodEmpresa", SqlDbType.Int);
                        param1.Value = reporte.CodigoEmpresa;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@MedioEnvio", SqlDbType.Int);
                        param1.Value = (object)reporte.MedioEnvio ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodAreaGen", SqlDbType.Int);
                        param1.Value = reporte.CodAreaGeneracion;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodGerenteGen", SqlDbType.Int);
                        param1.Value = reporte.CodGerenteGeneracion;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsableGen1", SqlDbType.Int);
                        param1.Value = reporte.CodResponsableGen1;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsableGen2", SqlDbType.Int);
                        param1.Value = reporte.CodResponsableGen2;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodAreaResponsable", SqlDbType.Int);
                        param1.Value = reporte.CodAreaResponsable;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodGerenteResponsable", SqlDbType.Int);
                        param1.Value = reporte.CodGerenteResponsable;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodCoordinador", SqlDbType.Int);
                        param1.Value = reporte.CodCoordinador;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsablePrincipal", SqlDbType.Int);
                        param1.Value = reporte.CodResponsablePrincipal;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsableAlterno", SqlDbType.Int);
                        param1.Value = (object)reporte.CodResponsableAlterno ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodFrecuencia", SqlDbType.Int);
                        param1.Value = reporte.CodFrecuencia;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@ProximoVencimiento", SqlDbType.DateTime);
                        param1.Value = reporte.ProximoVencimiento;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodTipoCalculo", SqlDbType.Int);
                        param1.Value = reporte.TipoCalculo;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Plazo", SqlDbType.Int);
                        param1.Value = reporte.Plazo;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@TipoDiasPlazo", SqlDbType.Int);
                        param1.Value = reporte.TipoDiasPlazo;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@DiasRecordatorio1", SqlDbType.Int);
                        param1.Value = reporte.DiasRecordatorio1;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@DiasRecordatorio2", SqlDbType.Int);
                        param1.Value = reporte.DiasRecordatorio2;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Estado", SqlDbType.Int);
                        param1.Value = reporte.Estado;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@UsuCreacion", SqlDbType.VarChar, 100);
                        param1.Value = reporte.UsuCreacion;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@UsuActualizacion", SqlDbType.VarChar, 100);
                        param1.Value = reporte.UsuActualizacion;
                        cmd.Parameters.Add(param1);

                        resultado = cmd.ExecuteNonQuery();

                        return resultado;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Reporte> ObtenerListaReporteBusqueda(string listaArea, DateTime? vencimientoDesde, DateTime? vencimientoHasta)
        {
            try
            {
                List<Reporte> reportes = new List<Reporte>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaReporteBusqueda]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@Areas", SqlDbType.VarChar, 50);
                        param1.Value = listaArea;
                        cmd.Parameters.Add(param1);
                        
                        param1 = new SqlParameter("@VctoDesde", SqlDbType.DateTime);
                        if (vencimientoDesde.HasValue)
                        {
                            param1.Value = vencimientoDesde.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }
                        
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@VctoHasta", SqlDbType.DateTime);

                        if (vencimientoHasta.HasValue)
                        {
                            param1.Value = vencimientoHasta.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }

                        cmd.Parameters.Add(param1);                        

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Reporte reporte = new Reporte();

                                if (!Convert.IsDBNull(reader["iCodReporte"]))
                                {
                                    reporte.CodigoReporte = Convert.ToInt32(reader["iCodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombre"]))
                                {
                                    reporte.NombreReporte = Convert.ToString(reader["vNombre"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    reporte.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodEmpresa"]))
                                {
                                    reporte.CodigoEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["Empresa"]))
                                {
                                    reporte.Empresa = Convert.ToString(reader["Empresa"]);
                                }

                                if (!Convert.IsDBNull(reader["iMedioEnvio"]))
                                {
                                    reporte.MedioEnvio = Convert.ToInt32(reader["iMedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["MedioEnvio"]))
                                {
                                    reporte.MedioEnvioDescripcion = Convert.ToString(reader["MedioEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaGen"]))
                                {
                                    reporte.CodAreaGeneracion = Convert.ToInt32(reader["iCodAreaGen"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaGeneracion"]))
                                {
                                    reporte.AreaGeneracion = Convert.ToString(reader["AreaGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteGen"]))
                                {
                                    reporte.CodGerenteGeneracion = Convert.ToInt32(reader["iCodGerenteGen"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteGeneracion"]))
                                {
                                    reporte.GerenteGeneracion = Convert.ToString(reader["GerenteGeneracion"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen1"]))
                                {
                                    reporte.CodResponsableGen1 = Convert.ToInt32(reader["iCodResponsableGen1"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion1"]))
                                {
                                    reporte.ResponsableGeneracion1 = Convert.ToString(reader["ResponsableGeneracion1"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableGen2"]))
                                {
                                    reporte.CodResponsableGen2 = Convert.ToInt32(reader["iCodResponsableGen2"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableGeneracion2"]))
                                {
                                    reporte.ResponsableGeneracion2 = Convert.ToString(reader["ResponsableGeneracion2"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodAreaResponsable"]))
                                {
                                    reporte.CodAreaResponsable = Convert.ToInt32(reader["iCodAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["AreaResponsable"]))
                                {
                                    reporte.AreaResponsable = Convert.ToString(reader["AreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteResponsable"]))
                                {
                                    reporte.CodGerenteResponsable = Convert.ToInt32(reader["iCodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["GerenteResponsable"]))
                                {
                                    reporte.GerenteResponsable = Convert.ToString(reader["GerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodCoordinador"]))
                                {
                                    reporte.CodCoordinador = Convert.ToInt32(reader["iCodCoordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["Coordinador"]))
                                {
                                    reporte.Coordinador = Convert.ToString(reader["Coordinador"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsablePrincipal"]))
                                {
                                    reporte.CodResponsablePrincipal = Convert.ToInt32(reader["iCodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsablePrincipal"]))
                                {
                                    reporte.ResponsablePrincipal = Convert.ToString(reader["ResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableAlterno"]))
                                {
                                    reporte.CodResponsableAlterno = Convert.ToInt32(reader["iCodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["ResponsableAlterno"]))
                                {
                                    reporte.ResponsableAlterno = Convert.ToString(reader["ResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodFrecuencia"]))
                                {
                                    reporte.CodFrecuencia = Convert.ToInt32(reader["iCodFrecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    reporte.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["dProximoVencimiento"]))
                                {
                                    reporte.ProximoVencimiento = Convert.ToDateTime(reader["dProximoVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodTipoCalculo"]))
                                {
                                    reporte.TipoCalculo = Convert.ToInt32(reader["iCodTipoCalculo"]);
                                }

                                if (!Convert.IsDBNull(reader["iPlazo"]))
                                {
                                    reporte.Plazo = Convert.ToInt32(reader["iPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iTipoDiasPlazo"]))
                                {
                                    reporte.TipoDiasPlazo = Convert.ToInt32(reader["iTipoDiasPlazo"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio1"]))
                                {
                                    reporte.DiasRecordatorio1 = Convert.ToInt32(reader["iDiasRecordatorio1"]);
                                }

                                if (!Convert.IsDBNull(reader["iDiasRecordatorio2"]))
                                {
                                    reporte.DiasRecordatorio2 = Convert.ToInt32(reader["iDiasRecordatorio2"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    reporte.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                reportes.Add(reporte);
                            }

                            return reportes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Reporte> ValidarNombreReporte(string nombreReporte)
        {
            try
            {
                List<Reporte> reportes = new List<Reporte>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ValidarNombreReporte]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@NombreReporte", SqlDbType.VarChar, 50);
                        param1.Value = nombreReporte;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Reporte reporte = new Reporte();

                                if (!Convert.IsDBNull(reader["iCodReporte"]))
                                {
                                    reporte.CodigoReporte = Convert.ToInt32(reader["iCodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombre"]))
                                {
                                    reporte.NombreReporte = Convert.ToString(reader["vNombre"]);
                                }

                                reportes.Add(reporte);
                            }

                            return reportes;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ActualizarReporteNotificacion(int codReporte, int tipoNotificacion)
        {
            try
            {
                int resultado = 0;

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ActualizarReporteNotificacion]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = codReporte;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@TipoNotificacion", SqlDbType.Int);
                        param1.Value = tipoNotificacion;
                        cmd.Parameters.Add(param1);

                        resultado = cmd.ExecuteNonQuery();

                        return resultado;
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
