﻿using Agrobanco.SCC.DAL.Entidades;
using System.Collections.Generic;
using System.IO;

namespace Agrobanco.SCC.DAL
{
    public class Laserfiche
    {
        public int codLaserFiche { get; set; }

        public RespuestaConsultaArchivosDTO ConsultaArchivos(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string archivoBase64 = null;
            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetDocument(item.Id, out fileName, out extension, out peso);
                    archivoBase64 = HelperIT.MemoryStreamToBase64(stream);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBase64 = archivoBase64,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }

        public string ConsultaArchivoSimple(int codigoSustento, out string fileName, out string extension)
        {
            string resultado = string.Empty;           
            decimal peso;

            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                var stream = laserFicheDocumentAccess.GetDocument(codigoSustento, out fileName, out extension, out peso);
                resultado = HelperIT.MemoryStreamToBase64(stream);

                laserFicheDocumentAccess.Dispose();
            }

            return resultado;
        }

        public RespuestaConsultaArchivosDTO ConsultaArchivosIMG(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string archivoBase64 = null;
            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetImage(item.Id, out fileName);
                    archivoBase64 = HelperIT.MemoryStreamToBase64(stream);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBase64 = archivoBase64,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }

        public RespuestaConsultaArchivosDTO ConsultaDefinicionArchivos(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    laserFicheDocumentAccess.GetDefinitionDocument(item.Id, out fileName, out extension, out peso);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }
        public RespuestaConsultaArchivosDTO ConsultaArchivosBytes(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetDocument(item.Id, out fileName, out extension, out peso);


                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBytes = stream.ToArray(),
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }

        public int CargarDocumentoLaserFiche(Stream stream, string fileExtension, string nombreArchivo, out string fileName)
        {
            int ldDocumentId = -1;
            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                stream.Position = 0;
                var ms = new MemoryStream();
                stream.CopyTo(ms);
                ms.Position = 0;
                var destinPath = "";
                var documentPath = "";
                
                destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}";
                lfhelper.GetOrCreateFolderInfo(destinPath);
                documentPath = $@"{destinPath}\{nombreArchivo}.{fileExtension}";                
                
                ldDocumentId = lfhelper.RegisterFileWithoutMetaData(ms, documentPath, "Default", out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

                lfhelper.Dispose();
            }
            return ldDocumentId;
        }

    }
}
