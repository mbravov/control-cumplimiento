﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.DAL
{
    public static class ApplicationKeys
    {
        public static string SqlRegeditFolder => ConfigurationManager.AppSettings["config:SqlApplicationName"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];
        public static string SQLApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:SQLApplicationNameEnableEncrip"];

        public static string LaserFicheServer => ConfigurationManager.AppSettings["config:Hostname"];
        public static string LaserFicheUser => ConfigurationManager.AppSettings["config:User"];
        public static string LaserFichePassword => ConfigurationManager.AppSettings["config:Password"];
        public static string LaserFicheRepository => ConfigurationManager.AppSettings["config:LfRepositorio"];
        public static string LaserFicheFolderBase => ConfigurationManager.AppSettings["config:LfFolderDocument"];

        public static string LaserFicheFolder => ConfigurationManager.AppSettings["config:LFApplicationName"];
        public static string LaserFicheEnableEncrip => ConfigurationManager.AppSettings["config:LaserFicheEnableEncrip"];
    }
}
