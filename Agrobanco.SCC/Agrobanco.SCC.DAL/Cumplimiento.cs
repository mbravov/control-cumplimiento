﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Agrobanco.SCC.DAL.Entidades;
using System.Data;

namespace Agrobanco.SCC.DAL
{
    public class Cumplimiento
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        public Cumplimiento(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        public List<Envio> ObtenerListaEnvio(int? codEnvio = null)
        {
            try
            {
                List<Envio> listaEnvio = new List<Envio>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaEnvio]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodEnvio", SqlDbType.Int);
                        param1.Value = (object)codEnvio ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Envio envio = new Envio();

                                if (!Convert.IsDBNull(reader["iCodEnvio"]))
                                {
                                    envio.CodEnvio = Convert.ToInt32(reader["iCodEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["CodReporte"]))
                                {
                                    envio.CodReporte = Convert.ToInt32(reader["CodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["NombreReporte"]))
                                {
                                    envio.NombreReporte = Convert.ToString(reader["NombreReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["CodEntidad"]))
                                {
                                    envio.CodEntidad = Convert.ToInt32(reader["CodEntidad"]);
                                }

                                if (!Convert.IsDBNull(reader["CodAreaResponsable"]))
                                {
                                    envio.CodAreaResponsable = Convert.ToInt32(reader["CodAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["CodGerenteResponsable"]))
                                {
                                    envio.CodGerenteResponsable = Convert.ToInt32(reader["CodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["CodResponsablePrincipal"]))
                                {
                                    envio.CodResponsablePrincipal = Convert.ToInt32(reader["CodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["CodResponsableAlterno"]))
                                {
                                    envio.CodResponsableAlterno = Convert.ToInt32(reader["CodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["FechaVencimiento"]))
                                {
                                    envio.FechaVencimiento = Convert.ToDateTime(reader["FechaVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["FechaEnvio"]))
                                {
                                    envio.FechaEnvio = Convert.ToDateTime(reader["FechaEnvio"]);
                                }
                                else
                                {
                                    envio.FechaEnvio = null;
                                }

                                if (!Convert.IsDBNull(reader["vMotivoIncumplimiento"]))
                                {
                                    envio.MotivoIncumplimiento = Convert.ToString(reader["vMotivoIncumplimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["EstadoEnvio"]))
                                {
                                    envio.CodEstadoEnvio = Convert.ToInt32(reader["EstadoEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["vComentarios"]))
                                {
                                    envio.Comentarios = Convert.ToString(reader["vComentarios"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodSustento"]))
                                {
                                    envio.CodSustento = Convert.ToInt32(reader["iCodSustento"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombreArchivo"]))
                                {
                                    envio.NombreArchivo = Convert.ToString(reader["vNombreArchivo"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    envio.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["vEmpresa"]))
                                {
                                    envio.Entidad = Convert.ToString(reader["vEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["vAreaResponsable"]))
                                {
                                    envio.AreaResponsable = Convert.ToString(reader["vAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["vGerenteResponsable"]))
                                {
                                    envio.GerenteResponsable = Convert.ToString(reader["vGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["vResponsablePrincipal"]))
                                {
                                    envio.ResponsablePrincipal = Convert.ToString(reader["vResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["vResponsableAlterno"]))
                                {
                                    envio.ResponsableAlterno = Convert.ToString(reader["vResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodVencimiento"]))
                                {
                                    envio.CodVencimiento = Convert.ToInt32(reader["iCodVencimiento"]);
                                }

                                listaEnvio.Add(envio);
                            }

                            return listaEnvio;
                       }                       
                    }
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public int RegistrarEnvio(Envio envio)
        {
            try
            {
                int resultado = 0;

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_RegistrarEnvio]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = envio.CodReporte;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@NombreReporte", SqlDbType.VarChar, 50);
                        param1.Value = (object)envio.NombreReporte ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Entidad", SqlDbType.VarChar, 300);
                        param1.Value = (object)envio.Entidad ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@AreaResponsable", SqlDbType.VarChar, 200);
                        param1.Value = (object)envio.AreaResponsable ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@GerenteResponsable", SqlDbType.VarChar, 400);
                        param1.Value = (object)envio.GerenteResponsable ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsablePrincipal", SqlDbType.Int);
                        param1.Value = envio.CodResponsablePrincipal;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@ResponsablePrincipal", SqlDbType.VarChar, 400);
                        param1.Value = (object)envio.ResponsablePrincipal ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodResponsableAlterno", SqlDbType.Int);
                        param1.Value = envio.CodResponsableAlterno;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@ResponsableAlterno", SqlDbType.VarChar, 400);
                        param1.Value = (object)envio.ResponsableAlterno;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@EstadoEnvio", SqlDbType.Int);
                        param1.Value = envio.CodEstadoEnvio;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@FechaVencimiento", SqlDbType.DateTime);

                        if (envio.FechaVencimiento.HasValue)
                        {
                            param1.Value = envio.FechaVencimiento.Value;
                        }
                        
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@FechaEnvio", SqlDbType.DateTime);

                        if (envio.FechaEnvio.HasValue)
                        {
                            param1.Value = envio.FechaEnvio.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }

                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@MotivoIncumplimiento", SqlDbType.VarChar, 200);
                        param1.Value = (object)envio.MotivoIncumplimiento ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Comentarios", SqlDbType.VarChar, 500);
                        param1.Value = (object)envio.Comentarios ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@CodSustento", SqlDbType.Int);
                        param1.Value = envio.CodSustento;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@NombreArchivo", SqlDbType.VarChar, 100);
                        param1.Value = (object)envio.NombreArchivo ?? DBNull.Value;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@UsuCreacion", SqlDbType.VarChar, 100);
                        param1.Value = envio.UsuarioCreacion;
                        cmd.Parameters.Add(param1);

                        resultado = cmd.ExecuteNonQuery();

                        return resultado;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Envio> ObtenerListaEnvioBusqueda(string listaEntidad, string listaFrecuencia, DateTime? vencimientoDesde, DateTime? vencimientoHasta)
        {
            try
            {
                List<Envio> envios = new List<Envio>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaEnvioBusqueda]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@Entidades", SqlDbType.VarChar, 50);
                        param1.Value = listaEntidad;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@Frecuencias", SqlDbType.VarChar, 50);
                        param1.Value = listaFrecuencia;
                        cmd.Parameters.Add(param1);                        

                        param1 = new SqlParameter("@VctoDesde", SqlDbType.DateTime);
                        if (vencimientoDesde.HasValue)
                        {
                            param1.Value = vencimientoDesde.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }
                        
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@VctoHasta", SqlDbType.DateTime);

                        if (vencimientoHasta.HasValue)
                        {
                            param1.Value = vencimientoHasta.Value;
                        }
                        else
                        {
                            param1.Value = DBNull.Value;
                        }

                        cmd.Parameters.Add(param1);                        

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Envio envio = new Envio();

                                if (!Convert.IsDBNull(reader["iCodEnvio"]))
                                {
                                    envio.CodEnvio = Convert.ToInt32(reader["iCodEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["CodReporte"]))
                                {
                                    envio.CodReporte = Convert.ToInt32(reader["CodReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["NombreReporte"]))
                                {
                                    envio.NombreReporte = Convert.ToString(reader["NombreReporte"]);
                                }

                                if (!Convert.IsDBNull(reader["vEmpresa"]))
                                {
                                    envio.Entidad = Convert.ToString(reader["vEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["vAreaResponsable"]))
                                {
                                    envio.AreaResponsable = Convert.ToString(reader["vAreaResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["vGerenteResponsable"]))
                                {
                                    envio.GerenteResponsable = Convert.ToString(reader["vGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["vResponsablePrincipal"]))
                                {
                                    envio.ResponsablePrincipal = Convert.ToString(reader["vResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["vResponsableAlterno"]))
                                {
                                    envio.ResponsableAlterno = Convert.ToString(reader["vResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstadoEnvio"]))
                                {
                                    envio.CodEstadoEnvio = Convert.ToInt32(reader["iEstadoEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodVencimiento"]))
                                {
                                    envio.CodVencimiento = Convert.ToInt32(reader["iCodVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["dFecVencimiento"]))
                                {
                                    envio.FechaVencimiento = Convert.ToDateTime(reader["dFecVencimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["dFecEnvio"]))
                                {
                                    envio.FechaEnvio = Convert.ToDateTime(reader["dFecEnvio"]);
                                }

                                if (!Convert.IsDBNull(reader["dFecRegistro"]))
                                {
                                    envio.FechaRegistro = Convert.ToDateTime(reader["dFecRegistro"]);
                                }

                                if (!Convert.IsDBNull(reader["vMotivoIncumplimiento"]))
                                {
                                    envio.MotivoIncumplimiento = Convert.ToString(reader["vMotivoIncumplimiento"]);
                                }

                                if (!Convert.IsDBNull(reader["vComentarios"]))
                                {
                                    envio.Comentarios = Convert.ToString(reader["vComentarios"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodSustento"]))
                                {
                                    envio.CodSustento = Convert.ToInt32(reader["iCodSustento"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombreArchivo"]))
                                {
                                    envio.NombreArchivo = Convert.ToString(reader["vNombreArchivo"]);
                                }

                                if (!Convert.IsDBNull(reader["Frecuencia"]))
                                {
                                    envio.Frecuencia = Convert.ToString(reader["Frecuencia"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsablePrincipal"]))
                                {
                                    envio.CodResponsablePrincipal = Convert.ToInt32(reader["iCodResponsablePrincipal"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodResponsableAlterno"]))
                                {
                                    envio.CodResponsableAlterno = Convert.ToInt32(reader["iCodResponsableAlterno"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGerenteResponsable"]))
                                {
                                    envio.CodGerenteResponsable = Convert.ToInt32(reader["iCodGerenteResponsable"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodCoordinador"]))
                                {
                                    envio.CodCoordinador = Convert.ToInt32(reader["iCodCoordinador"]);
                                }

                                envios.Add(envio);
                            }

                            return envios;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime ObtenerNuevaFechaVencimiento(int codReporte)
        {
            try
            {
                DateTime nuevaFechaVencimiento = new DateTime();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[UP_ObtenerNuevaFechaVencimiento]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = codReporte;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (!Convert.IsDBNull(reader["NuevaFechaVencimiento"]))
                                {
                                    nuevaFechaVencimiento = Convert.ToDateTime(reader["NuevaFechaVencimiento"]);
                                }                                
                            }

                            return nuevaFechaVencimiento;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ActualizarFechaVencimiento(int codReporte, DateTime fechaVencimiento, string usuActualizacion)
        {
            try
            {
                int resultado = 0;

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ActualizarFechaVencimientoReporte]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodReporte", SqlDbType.Int);
                        param1.Value = codReporte;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@FechaVencimiento", SqlDbType.DateTime);
                        param1.Value = fechaVencimiento;
                        cmd.Parameters.Add(param1);

                        param1 = new SqlParameter("@UsuActualizacion", SqlDbType.VarChar, 100);
                        param1.Value = usuActualizacion;
                        cmd.Parameters.Add(param1);

                        resultado = cmd.ExecuteNonQuery();

                        return resultado;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
