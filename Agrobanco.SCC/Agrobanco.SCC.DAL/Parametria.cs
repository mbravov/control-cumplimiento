﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Agrobanco.SCC.DAL.Entidades;
using System.Data;

namespace Agrobanco.SCC.DAL
{
    public class Parametria
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        public Parametria(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        public List<Parametro> ObtenerListaParametro(int codGrupo)
        {
            try
            {
                List<Parametro> parametros = new List<Parametro>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaParametro]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param1 = new SqlParameter("@CodGrupo", SqlDbType.Int);
                        param1.Value = codGrupo;
                        cmd.Parameters.Add(param1);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Parametro parametro = new Parametro();

                                if (!Convert.IsDBNull(reader["iCodParametro"]))
                                {
                                    parametro.CodigoParametro = Convert.ToInt32(reader["iCodParametro"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodGrupo"]))
                                {
                                    parametro.CodigoGrupo = Convert.ToInt32(reader["iCodGrupo"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodElemento"]))
                                {
                                    parametro.CodigoElemento = Convert.ToInt32(reader["iCodElemento"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    parametro.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["vValor"]))
                                {
                                    parametro.Valor = Convert.ToString(reader["vValor"]);
                                }

                                if (!Convert.IsDBNull(reader["iOrden"]))
                                {
                                    parametro.Orden = Convert.ToInt32(reader["iOrden"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    parametro.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                parametros.Add(parametro);
                            }

                            return parametros;
                       }                       
                    }
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public List<Parametro> ObtenerListaEmpresa()
        {
            try
            {
                List<Parametro> parametros = new List<Parametro>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaEmpresa]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Parametro parametro = new Parametro();

                                if (!Convert.IsDBNull(reader["iCodEmpresa"]))
                                {
                                    parametro.CodigoElemento = Convert.ToInt32(reader["iCodEmpresa"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    parametro.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    parametro.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                parametros.Add(parametro);
                            }

                            return parametros;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Parametro> ObtenerListaArea()
        {
            try
            {
                List<Parametro> parametros = new List<Parametro>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaArea]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Parametro parametro = new Parametro();

                                if (!Convert.IsDBNull(reader["iCodArea"]))
                                {
                                    parametro.CodigoElemento = Convert.ToInt32(reader["iCodArea"]);
                                }

                                if (!Convert.IsDBNull(reader["vDescripcion"]))
                                {
                                    parametro.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    parametro.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                parametros.Add(parametro);
                            }

                            return parametros;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuario> ObtenerListaUsuario()
        {
            try
            {
                List<Usuario> usuarios = new List<Usuario>();

                using (SqlConnection conn = new SqlConnection(cadenaConexion))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("[dbo].[USP_ObtenerListaUsuario]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Usuario usuario = new Usuario();

                                if (!Convert.IsDBNull(reader["iCodUsuario"]))
                                {
                                    usuario.CodUsuario = Convert.ToInt32(reader["iCodUsuario"]);
                                }

                                if (!Convert.IsDBNull(reader["iCodArea"]))
                                {
                                    usuario.CodArea = Convert.ToInt32(reader["iCodArea"]);
                                }

                                if (!Convert.IsDBNull(reader["vNombres"]))
                                {
                                    usuario.Nombres= Convert.ToString(reader["vNombres"]);
                                }

                                if (!Convert.IsDBNull(reader["vEmail"]))
                                {
                                    usuario.Email = Convert.ToString(reader["vEmail"]);
                                }

                                if (!Convert.IsDBNull(reader["vWEBUSER"]))
                                {
                                    usuario.WebUser = Convert.ToString(reader["vWEBUSER"]);
                                }

                                if (!Convert.IsDBNull(reader["vCargo"]))
                                {
                                    usuario.Cargo = Convert.ToString(reader["vCargo"]);
                                }

                                if (!Convert.IsDBNull(reader["iEstado"]))
                                {
                                    usuario.Estado = Convert.ToInt32(reader["iEstado"]);
                                }

                                usuarios.Add(usuario);
                            }

                            return usuarios;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
