﻿using Agrobanco.SCC.BLL;
using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SCC.WinService
{
    partial class CumplimientoService : ServiceBase
    {
        bool bandera = false;

        public CumplimientoService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            timer1.Start();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            timer1.Stop();
        }

        private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (bandera) return;

            string proceso = string.Empty;

            try
            {
                bandera = true;
                EventLog.WriteEntry("Se inicia el motor de notificaciones de cumplimiento", EventLogEntryType.Information);

                TimeSpan t = DateTime.Now.TimeOfDay;
                
                string horaInicio = ConfigurationManager.AppSettings["horaInicio"];
                string horaFin = ConfigurationManager.AppSettings["horaFin"];

                if (t.Hours >= Convert.ToInt32(horaInicio) && t.Hours <= Convert.ToInt32(horaFin))
                {
                    EventLog.WriteEntry("Hora correcta, procesar...", EventLogEntryType.Information);

                    ConfiguracionBLL configuration = new ConfiguracionBLL();
                    ParametriaBLL parametria = new ParametriaBLL();

                    var reportes = configuration.ObtenerListaReporteVencimiento();
                    var usersCumplimiento = parametria.ObtenerListaCorreosCumplimiento();
                    List <Reporte> reportesActivos = reportes.Where(r => r.Estado == 1).ToList();
                    List<string> emailCumplimiento = usersCumplimiento.Select(u => u.Descripcion).ToList();

                    EventLog.WriteEntry("Iniciando proceso Recordatorio 1", EventLogEntryType.Information);
                    proceso = "Recordatorio 1";
                    ProcesarRecordatorio1(reportesActivos, emailCumplimiento);

                    EventLog.WriteEntry("Iniciando proceso Recordatorio 2", EventLogEntryType.Information);
                    proceso = "Recordatorio 2";
                    ProcesarRecordatorio2(reportesActivos, emailCumplimiento);

                    EventLog.WriteEntry("Iniciando proceso Vencimiento", EventLogEntryType.Information);
                    proceso = "Vencimiento";
                    ProcesarVencimiento(reportesActivos, emailCumplimiento);

                    EventLog.WriteEntry("Iniciando proceso Incumplimiento", EventLogEntryType.Information);
                    proceso = "Incumplimiento";
                    ProcesarIncumplimiento(reportesActivos, emailCumplimiento);
                }

                EventLog.WriteEntry("Se finaliza el procesamiento del motor de notificaciones de cumplimiento", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry( "Proceso: " + proceso + " - Exception Message: " + ex.Message, EventLogEntryType.Error);
            }

            bandera = false;
        }

        private void ProcesarRecordatorio1(List<Reporte> reportes, List<string> usersCumplimiento)
        {
            List<int> frecuencias = new List<int>();
            frecuencias.Add((int)Enums.Frecuencia.Quincenal);
            frecuencias.Add((int)Enums.Frecuencia.Mensual);
            frecuencias.Add((int)Enums.Frecuencia.Trimestral);
            frecuencias.Add((int)Enums.Frecuencia.Cuatrimestral);
            frecuencias.Add((int)Enums.Frecuencia.Semestral);
            frecuencias.Add((int)Enums.Frecuencia.Anual);

            var reportesRec1 = reportes.Where(r => frecuencias.Contains(r.CodFrecuencia)).ToList();
                       
            foreach (Reporte item in reportesRec1)
            {
                if (item.Recordatorio1 || item.DiasRecordatorio1 == 0)
                    continue;

                DateTime vencimiento = item.ProximoVencimiento;
                DateTime fecRecordatorio1 = item.FechaRecordatorio1;

                if (DateTime.Now.Date == fecRecordatorio1.Date)
                {
                    //enviar correo;
                    string body = string.Empty;
                    string ruta = ConfigurationManager.AppSettings["rutaPlantillas"] + "Recordatorio.html";
                    using (StreamReader reader = new StreamReader(ruta))
                    {
                        body = reader.ReadToEnd();
                    }

                    body = AsignarValoresCuerpoCorreo(body, item);

                    EnvioCorreo envio = new EnvioCorreo();
                    List<string> listaPara = new List<string>();
                    List<string> listaCopia = new List<string>();
                    listaPara.Add(item.EmailResponsableAlterno);
                    listaPara.Add(item.EmailResponsablePrincipal);
                    listaCopia.Add(item.EmailRespGeneracion1);
                    listaCopia.Add(item.EmailCoordinador);
                    listaCopia.AddRange(usersCumplimiento);

                    envio.EnviarCorreo(listaPara, listaCopia, body);

                    //actualizar

                    ConfiguracionBLL configuration = new ConfiguracionBLL();

                    configuration.ActualizarReporteNotificacion(item.CodigoReporte, 1);
                }
            }
        }

        private void ProcesarRecordatorio2(List<Reporte> reportes, List<string> usersCumplimiento)
        {
            List<int> frecuencias = new List<int>();            
            frecuencias.Add((int)Enums.Frecuencia.Trimestral);
            frecuencias.Add((int)Enums.Frecuencia.Cuatrimestral);
            frecuencias.Add((int)Enums.Frecuencia.Semestral);
            frecuencias.Add((int)Enums.Frecuencia.Anual);

            var reportesRec2 = reportes.Where(r => frecuencias.Contains(r.CodFrecuencia)).ToList();

            foreach (Reporte item in reportesRec2)
            {
                if (item.Recordatorio2 || item.DiasRecordatorio2 == 0)
                    continue;

                DateTime vencimiento = item.ProximoVencimiento;
                DateTime fecRecordatorio2 = item.FechaRecordatorio2;

                if (DateTime.Now.Date == fecRecordatorio2.Date)
                {
                    //enviar correo;
                    string body = string.Empty;
                    string ruta = ConfigurationManager.AppSettings["rutaPlantillas"] + "Recordatorio.html";
                    using (StreamReader reader = new StreamReader(ruta))
                    {
                        body = reader.ReadToEnd();
                    }

                    body = AsignarValoresCuerpoCorreo(body, item);

                    EnvioCorreo envio = new EnvioCorreo();
                    List<string> listaPara = new List<string>();
                    List<string> listaCopia = new List<string>();
                    listaPara.Add(item.EmailResponsableAlterno);
                    listaPara.Add(item.EmailResponsablePrincipal);
                    listaCopia.Add(item.EmailRespGeneracion1);
                    listaCopia.Add(item.EmailCoordinador);
                    listaCopia.AddRange(usersCumplimiento);

                    envio.EnviarCorreo(listaPara, listaCopia, body);
                    
                    //actualizar
                    ConfiguracionBLL configuration = new ConfiguracionBLL();

                    configuration.ActualizarReporteNotificacion(item.CodigoReporte, 2);

                }
            }

        }

        private void ProcesarVencimiento(List<Reporte> reportes, List<string> usersCumplimiento)
        {
            foreach (Reporte item in reportes)
            {
                if (item.AlertaVencimiento)
                    continue;

                DateTime vencimiento = item.ProximoVencimiento;                

                if (DateTime.Now.Date == vencimiento.Date)
                {
                    //enviar correo;
                    string body = string.Empty;
                    string ruta = ConfigurationManager.AppSettings["rutaPlantillas"] + "Vencimiento.html";
                    using (StreamReader reader = new StreamReader(ruta))
                    {
                        body = reader.ReadToEnd();
                    }

                    body = AsignarValoresCuerpoCorreo(body, item);

                    EnvioCorreo envio = new EnvioCorreo();
                    List<string> listaPara = new List<string>();
                    List<string> listaCopia = new List<string>();
                    listaPara.Add(item.EmailResponsableAlterno);
                    listaPara.Add(item.EmailResponsablePrincipal);

                    listaCopia.Add(item.EmailRespGeneracion1);
                    listaCopia.Add(item.EmailCoordinador);
                    listaCopia.AddRange(usersCumplimiento);

                    envio.EnviarCorreo(listaPara, listaCopia, body);

                    //actualizar
                    ConfiguracionBLL configuration = new ConfiguracionBLL();

                    configuration.ActualizarReporteNotificacion(item.CodigoReporte, 3);
                }
            }
        }

        private void ProcesarIncumplimiento(List<Reporte> reportes, List<string> usersCumplimiento)
        {
            

            foreach (Reporte item in reportes)
            {
                if (item.AlertaIncumplimiento)
                    continue;

                DateTime vencimiento = item.ProximoVencimiento;

                if (DateTime.Now.Date > vencimiento.Date)
                {
                    //enviar correo;
                    string body = string.Empty;
                    string ruta = ConfigurationManager.AppSettings["rutaPlantillas"] + "Incumplimiento.html";

                    using (StreamReader reader = new StreamReader(ruta))
                    {
                        body = reader.ReadToEnd();
                    }

                    body = AsignarValoresCuerpoCorreo(body, item);

                    EnvioCorreo envio = new EnvioCorreo();
                    List<string> listaPara = new List<string>();
                    List<string> listaCopia = new List<string>();
                    listaPara.Add(item.EmailResponsableAlterno);
                    listaPara.Add(item.EmailResponsablePrincipal);

                    listaCopia.Add(item.EmailRespGeneracion1);
                    listaCopia.Add(item.EmailCoordinador);
                    listaCopia.Add(item.EmailGerenteResponsable);
                    listaCopia.AddRange(usersCumplimiento);

                    envio.EnviarCorreo(listaPara, listaCopia, body);

                    //actualizar
                    ConfiguracionBLL configuration = new ConfiguracionBLL();

                    //configuration.ActualizarReporteNotificacion(item.CodigoReporte, 4);
                }
            }
        }

        private string AsignarValoresCuerpoCorreo(string body, Reporte reporte)
        {
            body = body.Replace("{NombreReporte}", reporte.NombreReporte);
            body = body.Replace("{DescripcionReporte}", reporte.Descripcion);
            body = body.Replace("{Entidad}", reporte.Empresa);

            string fecha = reporte.ProximoVencimiento.Day.ToString().PadLeft(2, '0') + "/" + reporte.ProximoVencimiento.Month.ToString().PadLeft(2, '0') + "/" + reporte.ProximoVencimiento.Year.ToString();
            body = body.Replace("{FechaVencimiento}", fecha);

            return body;
        }
    }
}
