﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SCC.Web.Models
{
    public class EnvioModel
    {
        public int CodEnvio { get; set; }
        public int CodReporte { get; set; }
        public string NombreReporte { get; set; }
        public string DescripcionReporte { get; set; }
        public int CodEntidad { get; set; }
        public string Entidad { get; set; }
        public int CodAreaResponsable { get; set; }
        public string AreaResponsable { get; set; }
        public string CodResponsablePrincipal { get; set; }
        public string ResponsablePrincipal { get; set; }
        public string CodFrecuencia { get; set; }
        public string Frecuencia { get; set; }        
        public string FechaVencimiento { get; set; }
        public string FechaEnvio { get; set; }
        public string FechaRegistro { get; set; }

    }
}