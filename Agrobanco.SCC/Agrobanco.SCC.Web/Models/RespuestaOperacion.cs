﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SCC.Web.Models
{
    public class RespuestaOperacion
    {
        public RespuestaOperacion()
        {
            Resultado = 1;
        }

        public dynamic Data { get; set; } 

        public int Resultado { get; set; }

        public string Mensaje { get; set; }
    }
}