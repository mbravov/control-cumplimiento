﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SCC.Web.Models
{
    public class ReporteModel
    {
        public int CodReporte { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Entidad { get; set; }
        public string AreaResponsable { get; set; }
        public string ResponsablePrincipal { get; set; }
        public string Frecuencia { get; set; }
        public string Plazo { get; set; }
        public string ProximoVencimiento { get; set; }

    }
}