﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SCC.Web.Models
{
    public class ElementoBasico
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public bool Selected { get; set; }
        public bool Disabled { get; set; }
    }
}