﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Agrobanco.SCC.Web.Comun
{
    public static class ApplicationKeys
    {
        public static string UrlWebSSA => ConfigurationManager.AppSettings["UrlWebSSA"];
        public static string UrlBase => ConfigurationManager.AppSettings["UrlBase"];

        public static string UrlBaseIconoModulo => ConfigurationManager.AppSettings["UrlBaseIconoModulo"];

        public static string UrlAPISGS => ConfigurationManager.AppSettings["UrlAPISGS"];

        public static string APIrutaLogsError => ConfigurationManager.AppSettings["RutaLogs"];
        //public static int PageSize => HelpersComun.ConvertStringToInt(ConfigurationManager.AppSettings["PageSize"]);
    }
}