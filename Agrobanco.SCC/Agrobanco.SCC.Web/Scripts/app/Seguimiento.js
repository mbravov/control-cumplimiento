﻿let seguimiento;

$(document).ready(function () {
    "use strict";

    let Seguimiento = function () {
        let me = this;
        let appFunc = app.Funciones;
        me.Variables = {};

        me.Eventos = {
            BodyClick: function (event) {
                if (event.target.className != "select-dropdown dropdown-trigger") {
                    $("#cbo-area-responsable").formSelect();
                }
            },
            CboAreaResponsableChange: function () {
                var strListaArea = "";
                var str = "";
                $('#cbo-area-responsable option[value=""]').removeAttr('selected');
                $("#cbo-area-responsable option:selected").each(function () {
                    str += $(this).val() + ",";
                });

                if (str.length > 0) {
                    str = str.substring(0, str.length - 1);
                }
                //return str;
                //$('#cbo-area-responsable option[value=""]').removeAttr('selected');
                strListaArea = "0";

                if (str.length > 0) {
                    strListaArea = str;
                }

                $("#hdListaArea").val(strListaArea);
            },
            TxtVencimientoDesdeChange: function () {

                let fVctoDesde = $('#fecVencimientoDesde').val();
                let fechaDefault = new Date(fVctoDesde.split('/')[2], (fVctoDesde.split('/')[1] - 1), fVctoDesde.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoHasta'));

                instance.options.minDate = fechaDefault;

            },
            TxtVencimientoHastaChange: function () {
                let fVctoHasta = $('#fecVencimientoHasta').val();
                let fechaDefault = new Date(fVctoHasta.split('/')[2], (fVctoHasta.split('/')[1] - 1), fVctoHasta.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoDesde'));

                instance.options.maxDate = fechaDefault;
            }
        };
        me.Funciones = {
            InicializarEventos: function () {
                $("body").on("click", me.Eventos.BodyClick);
                $("body").on("change", "#cbo-area-responsable", me.Eventos.CboAreaResponsableChange);
                $("body").on("change", "#fecVencimientoDesde", me.Eventos.TxtVencimientoDesdeChange);
                $("body").on("change", "#fecVencimientoHasta", me.Eventos.TxtVencimientoHastaChange);
            },
            InicializarControles: function () {
                $("#estadoCumplimiento").val("4");
                $("#estadoCumplimiento").formSelect();
            },
            CargarInicio: function () {
                me.Funciones.InicializarControles();
            }
        };
        me.Init = function () {
            me.Funciones.InicializarEventos();
            me.Funciones.CargarInicio();
        }

    };

    seguimiento = new Seguimiento();

    seguimiento.Init();
});