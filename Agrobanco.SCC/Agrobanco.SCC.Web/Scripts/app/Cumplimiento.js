﻿let cumplimiento;

$(document).ready(function () {
    "use strict";

    let Cumplimiento = function () {
        let me = this;
        const urlBase = $("#hdUrlBase").val();        
        const codFrecuenciaDiaria = 1;
        const codFrecuenciaQuincenal = 2;
        const codFrecuenciaMensual = 3;
        const codFrecuenciaDemanda = 8;
        
        let modalEnvio = $('#modal-envio').modal(
            {
                dismissible: false,
                //onCloseEnd: function () {                    
                //    window.location.reload();                    
                //}
            });
        let hdTipoOperacion = $("#hdTipoOperacion");
        let hdCodigoEnvio = $("#hdCodigoEnvio");
        
        let appFunc = app.Funciones;        
        
        me.Variables = {            
            textModalTitulo: $('#textModalTitulo'),
            frmEnvioModal: $("#frmEnvioModal"),            
            cboReporte: $("#cboReporte"),
            cboEntidadModal: $("#cboEntidadModal"),
            cboAreaResModal: $("#cboAreaResModal"),
            cboGerenteResponsableModal: $("#cboGerenteResponsableModal"),
            cboResPrincipalModal: $("#cboResPrincipalModal"),
            cboResAlternoModal: $("#cboResAlternoModal"),
            dtProxVencimiento: $("#dt-prox-vencimiento"),
            cboEstadoEnvio: $("#cboEstadoEnvioModal"),
            dtFechaEnvio: $("#dt-fecha-envio"),
            cboMotivoModal: $("#cboMotivoModal"),
            divTextoDPlazo: $("#divTextoDPlazo"),
            divTextoFPlazo: $("#divTextoFPlazo"),
            divAcciones: $("#divAcciones"),
            nombreArchivo: $("#nombreArchivo"),
            sustentoEnvio: $("#sustentoEnvio")
        };

        me.Constantes = {
            tipoCalculoAutomatico: 1,
            tipoCalculoManual: 2
        };

        me.Eventos = {
            BtnNuevoClick: function () {
                me.Funciones.OpenEnvioModal(1);
            },
            BtnRegistrarEnvioModalClick: function (event) {

                if ((me.Variables.frmEnvioModal)[0].checkValidity()) {
                    event.preventDefault();
                }

                if (!(me.Variables.frmEnvioModal)[0].checkValidity()) {
                    return true;
                }

                me.Funciones.RegistrarEnvio();
            },
            BodyClick: function (event) {
                if (event.target.className != "select-dropdown dropdown-trigger") {
                    $("#cbo-entidad").formSelect();
                    $("#cbo-frecuencia").formSelect();
                }
            },
            CboEntidadChange: function () {

                var strListaEntidad = "";
                var str = "";
                $('#cbo-entidad option[value=""]').removeAttr('selected');
                $("#cbo-entidad option:selected").each(function () {
                    str += $(this).val() + ",";
                });

                if (str.length > 0) {
                    str = str.substring(0, str.length - 1);
                }
                //return str;
                //$('#cbo-area-responsable option[value=""]').removeAttr('selected');
                strListaEntidad = "0";

                if (str.length > 0) {
                    strListaEntidad = str;
                }

                $("#hdListaEntidad").val(strListaEntidad);
            },
            CboFrecuenciaChange: function () {

                var strListaFrecuencia = "";
                var str = "";
                $('#cbo-frecuencia option[value=""]').removeAttr('selected');
                $("#cbo-frecuencia option:selected").each(function () {
                    str += $(this).val() + ",";
                });

                if (str.length > 0) {
                    str = str.substring(0, str.length - 1);
                }
                
                strListaFrecuencia = "0";

                if (str.length > 0) {
                    strListaFrecuencia = str;
                }

                $("#hdListaFrecuencia").val(strListaFrecuencia);
            },
            TxtVencimientoDesdeChange: function () {

                let fVctoDesde = $('#fecVencimientoDesde').val();
                let fechaDefault = new Date(fVctoDesde.split('/')[2], (fVctoDesde.split('/')[1] - 1), fVctoDesde.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoHasta'));

                instance.options.minDate = fechaDefault;

            },
            TxtVencimientoHastaChange: function () {
                let fVctoHasta = $('#fecVencimientoHasta').val();
                let fechaDefault = new Date(fVctoHasta.split('/')[2], (fVctoHasta.split('/')[1] - 1), fVctoHasta.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoDesde'));

                instance.options.maxDate = fechaDefault;
            },
            FechaEnvioChange: function () {
                var instVencimiento = M.Datepicker.getInstance(document.getElementById("dt-prox-vencimiento"));
                var instEnvio = M.Datepicker.getInstance(document.getElementById("dt-fecha-envio"));
                
                var dentroDelPlazo = (instEnvio.date > instVencimiento.date) ? false : true;
                console.log("dentro del plazo: " + dentroDelPlazo.toString());

                if (dentroDelPlazo == false) {                    
                    me.Variables.cboMotivoModal.attr("required", true);
                    me.Variables.cboMotivoModal.addClass("select-require");
                }

                if (dentroDelPlazo == true) {                    
                    me.Variables.cboMotivoModal.removeAttr("required", true);
                    me.Variables.cboMotivoModal.removeClass("select-require");
                }
            },
            CboReporteChange: function (elem, event) {                
                var reporteSeleccionado = me.Variables.cboReporte.val();

                if (reporteSeleccionado != '') {
                    var optionSelected = $("#cboReporte option[value=" + reporteSeleccionado + "]");

                    var entidad = optionSelected.attr("data-entidad");
                    me.Variables.cboEntidadModal.val(entidad);
                    
                    var areaResponsable = optionSelected.attr("data-area-responsable");
                    me.Variables.cboAreaResModal.val(areaResponsable);
                    
                    var gerenteResponsable = optionSelected.attr("data-gerente-responsable");
                    me.Variables.cboGerenteResponsableModal.val(gerenteResponsable);
                    
                    var responsablePrincipal = optionSelected.attr("data-responsable-p");
                    me.Variables.cboResPrincipalModal.val(responsablePrincipal);
                    
                    var responsableAlterno = optionSelected.attr("data-responsable-a");
                    me.Variables.cboResAlternoModal.val(responsableAlterno);
                    
                    var diaVencimiento = optionSelected.attr("data-dia-vencimiento");
                    var mesVencimiento = optionSelected.attr("data-mes-vencimiento");
                    var anioVencimiento = optionSelected.attr("data-anio-vencimiento");
                    var proximoVencimiento = new Date(anioVencimiento, parseInt(mesVencimiento) - 1, diaVencimiento);                      
                    
                    M.Datepicker.getInstance(document.getElementById("dt-prox-vencimiento")).setDate(proximoVencimiento);
                    var strVencimiento = M.Datepicker.getInstance(document.getElementById("dt-prox-vencimiento")).toString();
                    me.Variables.dtProxVencimiento.val(strVencimiento);

                    $("label[for='dt-prox-vencimiento']").addClass("active");
                }

                if (reporteSeleccionado == '') {
                    me.Variables.cboEntidadModal.val('');
                    me.Variables.cboAreaResModal.val('');
                    me.Variables.cboGerenteResponsableModal.val('');
                    me.Variables.cboResPrincipalModal.val('');
                    me.Variables.cboResAlternoModal.val('');
                    me.Variables.dtProxVencimiento.val('');
                    $("label[for='dt-prox-vencimiento']").removeClass("active");
                }

                me.Variables.cboEntidadModal.formSelect();
                me.Variables.cboAreaResModal.formSelect();
                me.Variables.cboGerenteResponsableModal.formSelect();
                me.Variables.cboResPrincipalModal.formSelect();
                me.Variables.cboResAlternoModal.formSelect();
            },
            CboEstadoEnvioChange: function () {
                var estadoEnvio = me.Variables.cboEstadoEnvio.val();

                if (estadoEnvio == '0') {
                    me.Variables.dtFechaEnvio.removeAttr("required");                    
                    me.Variables.cboMotivoModal.attr("required", true);
                    me.Variables.cboMotivoModal.addClass("select-require");
                }

                if (estadoEnvio == '1') {
                    me.Variables.dtFechaEnvio.attr("required", true);
                    me.Variables.cboMotivoModal.removeAttr("required", true);
                    me.Variables.cboMotivoModal.removeClass("select-require");
                }
            },
            BtnExportarSustentoClick: function () {
                
                var codigoSustento = $("#hdCodigoSustento").val();
                location.href = "Cumplimiento/ExportarSustento?codigoSustento=" + codigoSustento;
                return false;
            },
            SustentoChange: function (event) {

                var file = event.target.files[0];               

                if (file == null)
                    return;

                if (!file.type.match('application/pdf')) {
                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("Solo se permiten archivos en formatos .PDF");
                    $('#ptv-alert-error').modal('open');
                    $("#sustentoEnvio").val('');
                    $("#sustentoEnvioAux").val('');
                    
                    return false;
                }

                if (file.size >= 5 * 1024 * 1024) {
                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("Tamaño máximo permitido 5MB");
                    $('#ptv-alert-error').modal('open');
                    $("#sustentoEnvio").val('');
                    $("#sustentoEnvioAux").val('');
                    
                    return false;
                }
            }
        };
        
        me.Funciones = {
            InicializarEventos: function () {
                $("body").on("click", me.Eventos.BodyClick);
                $("body").on("click", "#btn-nuevo", me.Eventos.BtnNuevoClick);
                $("body").on("click", "#btnRegistrarEnvioModal", me.Eventos.BtnRegistrarEnvioModalClick);
                //$("body").on("click", "#btnBuscar", me.Eventos.BtnBuscarModalClick);
                $("body").on("change", "#cbo-entidad", me.Eventos.CboEntidadChange);
                $("body").on("change", "#cbo-frecuencia", me.Eventos.CboFrecuenciaChange);
                $("body").on("change", "#fecVencimientoDesde", me.Eventos.TxtVencimientoDesdeChange);
                $("body").on("change", "#fecVencimientoHasta", me.Eventos.TxtVencimientoHastaChange);
                $("body").on("change", "#cboReporte", me.Eventos.CboReporteChange);
                $("body").on("change", "#cboEstadoEnvioModal", me.Eventos.CboEstadoEnvioChange);
                $("body").on("change", "#dt-fecha-envio", me.Eventos.FechaEnvioChange);
                $("body").on("click", "#btnExportarSustento", me.Eventos.BtnExportarSustentoClick);
                $("body").on("change", "#sustentoEnvio", me.Eventos.SustentoChange);
            },
            InicializarControles: function () {
                //$("#cbo-area-responsable").val("");

            },
            CargarInicio: function () {
                me.Funciones.InicializarControles();
            },
            OpenEnvioModal: function (modo, codEnvio) {
                hdTipoOperacion.val(modo);
                hdCodigoEnvio.val(codEnvio);
                modalEnvio.modal('open');

                if (modo == '1') {                    
                    me.Variables.textModalTitulo.text('Registro de Cumplimiento');                    
                    appFunc.ShowLoading();

                    me.Variables.divAcciones.show();
                    me.Variables.divTextoDPlazo.hide();
                    me.Variables.divTextoFPlazo.hide();
                    $("#sustentoEnvio").prop("required", true);
                    $("#sustentoEnvioAux").prop("required", true);
                    $("#divMotivoConsulta").hide();
                    $("#divMotivoNuevo").show();
                    $("#divSustentoConsulta").hide();
                    $("#divSustentoNuevo").show();
                    $("#cboReporte").removeAttr("disabled");
                    $("#cboEstadoEnvioModal").removeAttr("disabled");
                    $("#comentarios").removeAttr("disabled");
                    $("#dt-fecha-envio").removeAttr("disabled");
                    
                    $("select").formSelect();

                    $('#dt-fecha-envio').datepicker({
                        autoClose: true,
                        format: 'dd/mm/yyyy',
                        i18n: {
                            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                            weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                            weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                            weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"]
                        }                        
                    });

                    appFunc.LimpiarForm(me.Variables.frmEnvioModal);

                    appFunc.HideLoading();
                }

                if (modo == '2') {
                    me.Variables.textModalTitulo.text('Registro de Cumplimiento');
                    $("#divMotivoConsulta").show();
                    $("#divMotivoNuevo").hide();
                    $("#divSustentoConsulta").show();
                    $("#divSustentoNuevo").hide();
                    $("#sustentoEnvio").prop("required", false);
                    $("#sustentoEnvioAux").prop("required", false);
                    me.Variables.divAcciones.hide();
                    me.Variables.divTextoDPlazo.show();
                    me.Variables.divTextoFPlazo.show();

                    me.Funciones.ObtenerEnvio();
                }
            },
            RegistrarEnvio: function () {
                appFunc.ShowLoading();

                let url = '/Cumplimiento/RegistrarEnvio';

                var codigoEnvio = 0;

                if (hdTipoOperacion.val() == '2') {
                    codigoEnvio = hdCodigoEnvio.val();
                }

                var reporteSeleccionado = me.Variables.cboReporte.val();
                var optionSelected = $("#cboReporte option[value=" + reporteSeleccionado + "]");

                var nombreReporte = optionSelected.attr("data-nombre-reporte");
                var codTipoCalculo = optionSelected.attr("data-tipo-calculo");
                var codFrecuencia = optionSelected.attr("data-cod-frecuencia");

                var codEntidad = me.Variables.cboEntidadModal.val();
                var entidad = $("#cboEntidadModal option[value=" + codEntidad + "]").text();

                var codAreaResponsable = me.Variables.cboAreaResModal.val();
                var areaResponsable = $("#cboAreaResModal option[value=" + codAreaResponsable + "]").text();

                var codGerenteResponsable = me.Variables.cboGerenteResponsableModal.val();
                var gerenteResponsable = $("#cboGerenteResponsableModal option[value=" + codGerenteResponsable + "]").text();

                var codResponsablePrincipal = me.Variables.cboResPrincipalModal.val();
                var responsablePrincipal = $("#cboResPrincipalModal option[value=" + codResponsablePrincipal + "]").text();

                var codResponsableAlterno = me.Variables.cboResAlternoModal.val();
                var responsableAlterno = $("#cboResAlternoModal option[value=" + codResponsableAlterno + "]").text();

                var estadoEnvio = me.Variables.cboEstadoEnvio.val();
                var codMotivoIncumplimiento = me.Variables.cboMotivoModal.val();
                var motivoIncumplimiento = '';
                var instVencimiento = M.Datepicker.getInstance(document.getElementById("dt-prox-vencimiento"));
                var instEnvio = M.Datepicker.getInstance(document.getElementById("dt-fecha-envio"));
                
                var dentroDelPlazo = (instEnvio.date > instVencimiento.date) ? false : true;
                //var fVencimiento = me.Variables.dtProxVencimiento.val();
                //var fEnvio = me.Variables.dtFechaEnvio.val();

                if (estadoEnvio == '0' || !dentroDelPlazo)
                {
                    motivoIncumplimiento = $("#cboMotivoModal option[value=" + codMotivoIncumplimiento + "]").text();
                }

                var strComentarios = $("#comentarios").val();

                let fileSustento = document.getElementById('sustentoEnvio');
                let i = 0;                

                const sendData = new FormData();
                sendData.append('CodReporte', reporteSeleccionado);
                sendData.append('NombreReporte', nombreReporte);
                sendData.append('Entidad', entidad);
                sendData.append('AreaResponsable', areaResponsable);
                sendData.append('GerenteResponsable', gerenteResponsable);
                sendData.append('CodResponsablePrincipal', codResponsablePrincipal);
                sendData.append('ResponsablePrincipal', responsablePrincipal);
                sendData.append('CodResponsableAlterno', codResponsableAlterno);
                sendData.append('ResponsableAlterno', responsableAlterno);
                sendData.append('CodEstadoEnvio', me.Variables.cboEstadoEnvio.val());
                sendData.append('FechaVencimiento', me.Variables.dtProxVencimiento.val());
                sendData.append('FechaEnvio', me.Variables.dtFechaEnvio.val());
                sendData.append('CodTipoCalculo', codTipoCalculo);
                sendData.append('CodFrecuencia', codFrecuencia);
                sendData.append('MotivoIncumplimiento', motivoIncumplimiento);

                sendData.append('Comentarios', strComentarios);
                for (i = 0; i < fileSustento.files.length; i++) {
                    sendData.append(fileSustento.files[i].name, fileSustento.files[i]);
                }

                appFunc.EjecutarAjax('POST', me, url, sendData,
                    function (result) {
                        
                        appFunc.HideLoading();
                        if (result.Resultado == '0') {
                            appFunc.msgError(result.Mensaje);                            
                        }
                        
                        if (result.Resultado == '1') {

                            var fechaVencimiento = result.Data.strFechaVencimiento;
                            var msje = "Se ha registrado correctamente el cumplimiento";
                            var msj2 = "";
                            var msj3 = "";

                            if (codTipoCalculo == me.Constantes.tipoCalculoAutomatico) {
                                msj2 = "Se ha establecido la nueva fecha de vencimiento del reporte en: " + fechaVencimiento.toString();
                            }

                            if (codTipoCalculo == me.Constantes.tipoCalculoManual) {
                                msj2 = "El cálculo de fecha de vencimiento de este reporte es manual, por favor, comuníquese con el área de Cumplimiento Normativo para su debida actualización.";
                            }

                            if (dentroDelPlazo) {

                                msj3 = "¡Ha enviado el reporte en el plazo correcto!";
                                me.Funciones.msgInformacionDPlazo(msje, msj2, msj3, function () {
                                    modalEnvio.modal('close');
                                    location.reload();
                                });
                            }

                            if (!dentroDelPlazo) {

                                msj3 = "¡Ha enviado el reporte fuera del plazo!";

                                me.Funciones.msgInformacionFPlazo(msje, msj2, msj3, function () {
                                    modalEnvio.modal('close');
                                    location.reload();
                                });
                            }
                        }                        
                    },
                    function (e, b, c) { }
                );
                
            },
            msgInformacionDPlazo: function (mensaje, mensaje2, mensaje3, function_close) {
                $('#ptv-alert-dplazo-ok').modal({
                    onCloseEnd: function_close
                });
                $('.p-descripcion').text(mensaje);
                $('.p-otradescripcion').text(mensaje2);
                $('.texto-mensaje-plazo').text(mensaje3);
                $('#ptv-alert-dplazo-ok').modal('open');
            },
            msgInformacionFPlazo: function (mensaje, mensaje2, mensaje3, function_close) {
                $('#ptv-alert-fplazo-ok').modal({
                    onCloseEnd: function_close
                });
                $('.p-descripcion').text(mensaje);
                $('.p-otradescripcion').text(mensaje2);
                $('.texto-mensaje-plazo').text(mensaje3);                
                $('#ptv-alert-fplazo-ok').modal('open');
            },
            ObtenerEnvio: function () {
                const sendData = new FormData();
                let codEnvio = hdCodigoEnvio.val();
                let urlGet = '/Cumplimiento/ObtenerEnvio?codEnvio=' + codEnvio;

                appFunc.EjecutarAjax('GET', me, urlGet, sendData,
                    function (result) {
                        
                        if (result.Resultado == '0') {
                            appFunc.HideLoading();
                            appFunc.msgError(result.Mensaje);                            
                        }

                        if (result.Resultado == '1') {
                            var envio = result.Data.envio;                            
                            var dentroDelPlazo = result.Data.dentroDelPlazo;
                            
                            modalEnvio.parent().find("label").addClass("active");

                            $("#cboReporte").val(envio.CodReporte);
                            $("#cboReporte").prop("disabled", true);
                            $("#cboEntidadModal").val(envio.CodEntidad);
                            $("#cboAreaResModal").val(envio.CodAreaResponsable);
                            $("#cboGerenteResponsableModal").val(envio.CodGerenteResponsable);
                            $("#cboResPrincipalModal").val(envio.CodResponsablePrincipal);
                            $("#cboResAlternoModal").val(envio.CodResponsableAlterno);
                            $("#dt-prox-vencimiento").val(appFunc.ParseDatetime(envio.FechaVencimiento));
                            $("#cboEstadoEnvioModal").val(envio.CodEstadoEnvio);
                            $("#cboEstadoEnvioModal").prop("disabled", true);
                            
                            $("#dt-fecha-envio").val(appFunc.ParseDatetime(envio.FechaEnvio));
                            $("#dt-fecha-envio").prop("disabled", true);
                            $("#motivos").val(envio.MotivoIncumplimiento);
                            $("#comentarios").val(envio.Comentarios);
                            $("#comentarios").prop("disabled", true);

                            $("#hdCodigoSustento").val(envio.CodSustento);
                            me.Variables.nombreArchivo.text(envio.NombreArchivo);
                            me.Variables.divTextoDPlazo.hide();
                            me.Variables.divTextoFPlazo.hide();

                            if (envio.CodEstadoEnvio == 1)
                            {
                                if (dentroDelPlazo) {
                                    me.Variables.divTextoDPlazo.show();
                                    me.Variables.divTextoFPlazo.hide();
                                }

                                if (!dentroDelPlazo) {
                                    me.Variables.divTextoDPlazo.hide();
                                    me.Variables.divTextoFPlazo.show();
                                }
                            }

                            $("select").formSelect();

                            appFunc.HideLoading();
                        }
                    }
                );
            }
        };

        me.Init = function () {
            me.Funciones.InicializarEventos();
            me.Funciones.CargarInicio();
        };
    };

    cumplimiento = new Cumplimiento();

    cumplimiento.Init();
});
