﻿let app;

$(document).ready(function () {
    "use strict";

    let App = function () {
        let me = this;
        const urlBase = $("#hdUrlBase").val();

        me.Variables = {
            unavezMenu: true
        };

        me.Constantes = {};

        me.Eventos = {
            MenuOpenClick: function () {
                me.Funciones.ShowSidebar();
            },
            SideNavOverlayClick: function () {
                me.Funciones.HideSideBar();
            }
        };

        me.Funciones = {
            InicializarEventos: function () {
                $("body").on("click", "#menu-open", me.Eventos.MenuOpenClick);
                $("body").on("click", ".menu-li", me.Eventos.MenuOpenClick);
                $("body").on("click", ".sidenav-overlay", me.Eventos.SideNavOverlayClick);
            },
            InicializarControles: function () {
                $('.collapsible').collapsible();
                $('select').formSelect();
                //$('.tabs').tabs();
                $('.modal').modal();
                $('.datepicker').datepicker({
                    autoClose: true,
                    showClearBtn: true,
                    format: 'dd/mm/yyyy',
                    i18n: {
                        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                        weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                        weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"]
                    }
                });

            },
            CargarInicio: function () {
                me.Funciones.InicializarControles();
            },
            ShowSidebar: function () {
                $("body").addClass("menu-open");
                $('.sidenav-overlay').attr('style', 'display: block; opacity: 1;');

                if (me.Variables.unavezMenu) {
                    $(".collapsible-body.active").parent().addClass("active").find(".collapsible-body").css("display", "block");
                    me.Variables.unavezMenu = false;
                }
            },
            HideSideBar: function () {
                $("body").removeClass('menu-open');
                $('.sidenav-overlay').removeAttr('style');
                $(".collapsible-body").css("display", "none");
                $(".menu-li").removeClass("active");
            },
            EjecutarAjax: function (tipo, obj, url, sendData, function_ok, function_error) {
                //$("#modal_loading_agro").attr("style", "display:block");
                $(obj).prop("disabled", true);                
                $.ajax({
                    type: tipo,
                    enctype: 'multipart/form-data',
                    url: urlBase + url,
                    data: sendData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (result) {
                        //try {
                        //    if (result.includes('Operacion no authorizada')) {
                        //        location.reload(true);
                        //    } else {
                        //        function_ok(result);
                        //        $(obj).prop("disabled", false);
                        //    };
                        //}
                        //catch (err) {
                        //    function_ok(result);
                        //    $(obj).prop("disabled", false);
                        //}; 
                        function_ok(result);
                        $(obj).prop("disabled", false);
                    },
                    complete: function () {
                        //$('#modal_loading_agro').attr("style", "display:none");
                        $(obj).prop("disabled", false);
                    },
                    error: function (e, b, c) {
                        if (e.status == 401) {
                            location.reload(true);
                        } else {
                            function_error(e, b, c);
                            $(obj).prop("disabled", false);
                        };
                    }
                });
            },
            ShowLoading: function () {
                $('#modal_loading_agro').attr("style", "display:block");
            },
            HideLoading: function () {
                $('#modal_loading_agro').attr("style", "display:none");
            },
            DisableInput: function (item) {
                item.prop("disabled", true);
            },
            EnableInput: function (item) {
                item.prop("disabled", false);
            },
            LimpiarForm: function (item) {
                item.parent().find("input").val("");
                item[0].reset();
                item.parent().find("[id*=-error]").attr("style", "display:none");
            },
            AddStyle: function (item, style) {
                item.attr("style", style);
            },
            LoadMultipleSelect: function (List, nameItem) {
                if (List.length > 0) {
                    $('#' + nameItem + ' option[value=""]').removeAttr('selected');

                    $.each(List, function (i, e) {
                        $("#" + nameItem + " option[value='" + e + "']").prop("selected", true);
                    });
                }
            },
            ValSoloNumeros: function (value) {
                if (/\D/g.test(value)) {
                    return value.replace(/\D/g, '');
                } else {
                    return value;
                }
            },
            ValSoloNumerosLetras: function (value) {
                var regExp = /^[0-9a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ]+$/;
                var result = regExp.test(value);
                if (result == false) {
                    value = RemoveCaracterInvalido(value);
                }
                return value;
            },
            RemoveCaracterInvalido: function (value) {
                return value.substr(0, value.length - 1);
            },
            ParseDatetime: function (date) {
                if (date != null) {
                    let fecha = date.replace(/\/Date\((-?\d+)\)\//, '$1');
                    let _date = new Date(parseInt(fecha));
                    let day = _date.getDate();
                    let month = _date.getMonth() + 1;
                    let year = _date.getFullYear();

                    if (day < 10) {
                        day = "0" + day;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }

                    let dateFinal = day + "/" + month + "/" + year;
                    return dateFinal;
                } else {
                    return "";
                }

            },
            msgError: function (mensaje) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
            },
            msgInformacion: function(mensaje, function_close) {
                $('#ptv-alert-ok').modal({
                    onCloseEnd: function_close
                });
                $('.p-descripcion').text(mensaje);
                $('#ptv-alert-ok').modal('open');
            },
            msgInformacion2: function (mensaje, mensaje2, function_close) {
                $('#ptv-alert-ok').modal({
                    onCloseEnd: function_close
                });
                $('.p-descripcion').text(mensaje);
                $('.p-otradescripcion').text(mensaje2);
                $('#ptv-alert-ok').modal('open');
            }
        };

        me.Init = function () {
            me.Funciones.InicializarEventos();
            me.Funciones.CargarInicio();
        };
    };

    app = new App();

    app.Init();
});
