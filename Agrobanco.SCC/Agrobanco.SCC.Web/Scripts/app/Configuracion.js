﻿let configuracion;

$(document).ready(function () {
    "use strict";

    let Configuracion = function () {
        let me = this;
        const urlBase = $("#hdUrlBase").val();
        const displayBlock = "display:block";
        const displayNone = "display:none";
        const codFrecuenciaDiaria = 1;
        const codFrecuenciaQuincenal = 2;
        const codFrecuenciaMensual = 3;
        const codFrecuenciaDemanda = 8;

        //let modalReporte = $('#modal-reporte').modal({ dismissible: false, onCloseEnd: function () { debugger; location.reload(); } });
        let modalReporte = $('#modal-reporte').modal(
            {
                dismissible: false,
                //onCloseEnd: function () {                    
                //    window.location.reload();                    
                //}            
            });
        let hdTipoOperacion = $("#hdTipoOperacion");
        let hdCodigoReporte = $("#hdCodigoReporte");
        
        let appFunc = app.Funciones;
        let modal_loading_agro = $('#modal_loading_agro');        
        
        me.Variables = {            
            textModalRepTitulo: $('#textModalRepTitulo'),
            frmReporteModal: $("#frmReporteModal"),
            txtDiasRecordatorio1: $("#diasRecordatorio1Modal"),
            txtDiasRecordatorio2: $("#diasRecordatorio2Modal"),
            cboFrecuencia: $("#cboFrecuenciaModal"),
            cboEstadoModal: $("#cboEstadoModal")
        };

        me.Constantes = {};

        me.Eventos = {
            BtnNuevoClick: function () {
                me.Funciones.OpenReporteModal(1);
                $("#btnCancelarReporteModal").val("Cancelar");
            },
            BtnGuardarReporteModalClick: function (event) {

                if ((me.Variables.frmReporteModal)[0].checkValidity()) {
                    event.preventDefault();
                }

                if (!(me.Variables.frmReporteModal)[0].checkValidity()) {
                    return true;
                }

                me.Funciones.GuardarReporte();
            },
            BodyClick: function (event) {
                if (event.target.className != "select-dropdown dropdown-trigger") {
                    $("#cbo-area-responsable").formSelect();
                }
            },
            CboAreaResponsableChange: function () {

                var strListaArea = "";
                var str = "";
                $('#cbo-area-responsable option[value=""]').removeAttr('selected');
                $("#cbo-area-responsable option:selected").each(function () {
                    str += $(this).val() + ",";
                });

                if (str.length > 0) {
                    str = str.substring(0, str.length - 1);
                }
                //return str;
                //$('#cbo-area-responsable option[value=""]').removeAttr('selected');
                strListaArea = "0";

                if (str.length > 0) {
                    strListaArea = str;
                }

                $("#hdListaArea").val(strListaArea);
            },
            TxtVencimientoDesdeChange: function () {

                let fVctoDesde = $('#fecVencimientoDesde').val();
                let fechaDefault = new Date(fVctoDesde.split('/')[2], (fVctoDesde.split('/')[1] - 1), fVctoDesde.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoHasta'));

                instance.options.minDate = fechaDefault;

            },
            TxtVencimientoHastaChange: function () {
                let fVctoHasta = $('#fecVencimientoHasta').val();
                let fechaDefault = new Date(fVctoHasta.split('/')[2], (fVctoHasta.split('/')[1] - 1), fVctoHasta.split('/')[0]);

                var instance = M.Datepicker.getInstance($('#fecVencimientoDesde'));

                instance.options.maxDate = fechaDefault;
            }
        };
        
        me.Funciones = {
            InicializarEventos: function () {
                $("body").on("click", me.Eventos.BodyClick);
                $("body").on("click", "#btn-nuevo", me.Eventos.BtnNuevoClick);
                $("body").on("click", "#btnGuardarReporteModal", me.Eventos.BtnGuardarReporteModalClick);                
                //$("body").on("click", "#btnBuscar", me.Eventos.BtnBuscarModalClick);
                $("body").on("change", "#cbo-area-responsable", me.Eventos.CboAreaResponsableChange);
                $("body").on("change", "#fecVencimientoDesde", me.Eventos.TxtVencimientoDesdeChange);
                $("body").on("change", "#fecVencimientoHasta", me.Eventos.TxtVencimientoHastaChange);
            },
            InicializarControles: function () {
                //$("#cbo-area-responsable").val("");

            },
            CargarInicio: function () {
                me.Funciones.InicializarControles();
            },
            OpenReporteModal: function (modo, codReporte) {
                hdTipoOperacion.val(modo);
                hdCodigoReporte.val(codReporte);
                appFunc.ShowLoading();
                modalReporte.modal('open');

                if (modo == '1') {                    
                    me.Variables.textModalRepTitulo.text('Nuevo Reporte');
                    me.Variables.cboEstadoModal.parent().prop("disabled", true);

                    $('#dt-prox-vencimiento').datepicker({
                        autoClose: true,
                        format: 'dd/mm/yyyy',
                        i18n: {
                            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                            weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                            weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                            weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"]
                        },
                        onOpen: function () {
                            var instance = M.Datepicker.getInstance($('#dt-prox-vencimiento'));                            

                            var year = (new Date()).getFullYear();
                            var month = (new Date()).getMonth();
                            var day = (new Date()).getDate();

                            instance.options.minDate = new Date(year, month, day);
                        }
                    });

                    appFunc.LimpiarForm(me.Variables.frmReporteModal);

                    appFunc.HideLoading();
                }

                if (modo == '2') {
                    me.Variables.textModalRepTitulo.text('Editar Reporte');
                    me.Funciones.ObtenerReporte();
                }
            },
            GuardarReporte: function () {
                appFunc.ShowLoading();

                let url = '/Configuracion/RegistrarReporte';

                var codigoReporte = 0;

                if (hdTipoOperacion.val() == '2') {
                    codigoReporte = hdCodigoReporte.val();
                }
                
                const sendData = new FormData();
                sendData.append('CodigoReporte', codigoReporte);
                sendData.append('NombreReporte', $("#nombreModal").val());
                sendData.append('Descripcion', $("#descripcionModal").val());
                sendData.append('CodigoEmpresa', $("#cboEntidadModal").val());
                sendData.append('MedioEnvio', $("#cboMedioEnvioModal").val());
                sendData.append('CodAreaGeneracion', $("#cboAreaGenModal").val());
                sendData.append('CodGerenteGeneracion', $("#cboGerenteGenModal").val());
                sendData.append('CodResponsableGen1', $("#cboResGen1Modal").val());
                sendData.append('CodResponsableGen2', $("#cboResGen2Modal").val());
                sendData.append('CodAreaResponsable', $("#cboAreaResModal").val());
                sendData.append('CodGerenteResponsable', $("#cboGerenteResponsableModal").val());
                sendData.append('CodCoordinador', $("#cboCoordinadorModal").val());
                sendData.append('CodResponsablePrincipal', $("#cboResPrincipalModal").val());
                sendData.append('CodResponsableAlterno', $("#cboResAlternoModal").val());
                sendData.append('CodFrecuencia', $("#cboFrecuenciaModal").val());

                let proxVencimiento = $("#dt-prox-vencimiento").val();
                let diaVenc = proxVencimiento.split('/')[0];
                let mesVenc = proxVencimiento.split('/')[1];
                let anioVenc = proxVencimiento.split('/')[2];

                sendData.append('ProximoVencimiento', $("#dt-prox-vencimiento").val());

                sendData.append('TipoCalculo', $("#cboTipoCalculoModal").val());
                sendData.append('Plazo', $("#plazoModal").val());
                sendData.append('TipoDiasPlazo', $("#cboTipoDiasPlazoModal").val());
                sendData.append('DiasRecordatorio1', $("#diasRecordatorio1Modal").val());
                sendData.append('DiasRecordatorio2', $("#diasRecordatorio2Modal").val());
                sendData.append('Estado', $("#cboEstadoModal").val());
                
                appFunc.EjecutarAjax('POST', me, url, sendData,
                    function (result) {
                        
                        appFunc.HideLoading();
                        if (result.Resultado == '0') {
                            appFunc.msgError(result.Mensaje);                            
                        }
                        
                        if (result.Resultado == '1') {
                            
                            var msj2 = "";
                            var msje = "Se ha registrado correctamente el reporte";

                            if (hdTipoOperacion.val() == '2') {
                                msje = "Se ha actualizado correctamente el reporte";
                            }

                            if (me.Variables.cboFrecuencia.val() == codFrecuenciaDiaria)
                            {
                                msj2 = "El recordatorio 1 y recordatorio 2 no aplica para reportes con frecuencia Diaria";
                            }

                            if (me.Variables.cboFrecuencia.val() == codFrecuenciaQuincenal || me.Variables.cboFrecuencia.val() == codFrecuenciaMensual)
                            {
                                msj2 = "El recordatorio 2 no aplica para reportes con frecuencia quincenal y mensual";
                            }

                            if (me.Variables.cboFrecuencia.val() == codFrecuenciaDemanda)
                            {
                                msj2 = "No aplican los recordatorios para la frecuencia A demanda";
                            }
                            
                            appFunc.msgInformacion2(msje, msj2, function () {
                                modalReporte.modal('close');
                                location.reload();
                            });

                        }                        
                    },
                    function (e, b, c) { }
                );

                //appFunc.HideLoading();
            },
            ObtenerReporte: function () {
                const sendData = new FormData();
                let codReporte = hdCodigoReporte.val();
                let urlGet = '/Configuracion/ObtenerReporte?codigoReporte=' + codReporte;

                appFunc.EjecutarAjax('GET', me, urlGet, sendData,
                    function (result) {
                        
                        if (result.Resultado == '0') {
                            appFunc.HideLoading();
                            appFunc.msgError(result.Mensaje);
                        }

                        if (result.Resultado == '1') {
                            var reporte = result.Data;

                            modalReporte.parent().find("label").addClass("active");

                            $("#nombreModal").val(reporte.NombreReporte);
                            $("#descripcionModal").val(reporte.Descripcion);
                            $("#cboEntidadModal").val(reporte.CodigoEmpresa);
                            $("#cboMedioEnvioModal").val(reporte.MedioEnvio);
                            $("#cboAreaGenModal").val(reporte.CodAreaGeneracion);
                            $("#cboGerenteGenModal").val(reporte.CodGerenteGeneracion);
                            $("#cboResGen1Modal").val(reporte.CodResponsableGen1);
                            $("#cboResGen2Modal").val(reporte.CodResponsableGen2);
                            $("#cboAreaResModal").val(reporte.CodAreaResponsable);

                            $("#cboGerenteResponsableModal").val(reporte.CodGerenteResponsable);
                            $("#cboCoordinadorModal").val(reporte.CodCoordinador);
                            $("#cboResPrincipalModal").val(reporte.CodResponsablePrincipal);
                            $("#cboResAlternoModal").val(reporte.CodResponsableAlterno);
                            $("#cboFrecuenciaModal").val(reporte.CodFrecuencia);
                            $("#dt-prox-vencimiento").val(appFunc.ParseDatetime(reporte.ProximoVencimiento));
                            $("#cboTipoCalculoModal").val(reporte.TipoCalculo);
                            $("#plazoModal").val(reporte.Plazo);
                            $("#cboTipoDiasPlazoModal").val(reporte.TipoDiasPlazo);
                            $("#diasRecordatorio1Modal").val(reporte.DiasRecordatorio1);
                            $("#diasRecordatorio2Modal").val(reporte.DiasRecordatorio2);
                            $("#cboEstadoModal").val(reporte.Estado);
                            $("#cboEstadoModal").removeAttr("disabled");                            

                            $("select").formSelect();

                            appFunc.HideLoading();
                        }
                    }

                );
            }            
        };

        me.Init = function () {
            me.Funciones.InicializarEventos();
            me.Funciones.CargarInicio();
        };
    };

    configuracion = new Configuracion();

    configuracion.Init();
});
