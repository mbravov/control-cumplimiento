﻿using System.Web;
using System.Web.Optimization;

namespace Agrobanco.SCC.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/js/app").Include(
                      "~/Scripts/app/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/main.css"));

            bundles.Add(new StyleBundle("~/Content/css/material").Include(
                //"~/Content/lib/materialize/css/materialize.css",
                "~/Content/lib/materialize/css/materialize.min.css",
                "~/Content/lib/material-icon/material-icons.css"));

            bundles.Add(new ScriptBundle("~/bundles/js/material").Include(
                //"~/Content/lib/materialize/js/materialize.js",
                "~/Content/lib/materialize/js/materialize.min.js"));
        }
    }
}
