﻿using Agrobanco.SCC.Web.Comun;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SCC.Web.Controllers
{
    public class BaseController : Controller
    {        
        public void GenerarExcelResponse<T>(string fileName, string worksheetName, T data, Action<IXLWorksheet, T> writeData) where T : class
        {
            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add(worksheetName);

            //var imagePath = Server.MapPath("~/Content/img/logo-principal.png");

            //worksheet.AddPicture(imagePath)
            //    .MoveTo(worksheet.Cell("A1"))
            //    .Scale(0.5);

            writeData(worksheet, data);


            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", fileName));

            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            workbook.Dispose();

            Response.End();
        }

        public void GenerarLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = ApplicationKeys.APIrutaLogsError;
            logFilePath = logFilePath + "LogCumplimiento-" + System.DateTime.Today.ToString("dd-MM-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }
    }
}