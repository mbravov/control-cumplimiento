﻿using Agrobanco.SCC.Web.Comun;
using System;
using Agrobanco.SCC.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agrobanco.SCC.Web.Helpers;

namespace Agrobanco.SCC.Web.Controllers
{
    public class RedirectController : Controller
    {

        [HttpGet]
        public ActionResult RedirectExternalUrl(string opcionRedirect)
        {
            if (opcionRedirect == "1" || string.IsNullOrEmpty(opcionRedirect) || Session["TokenSCC"] == null)
            {
                Session.Clear();
                Session.Abandon();
                Uri Url = Request.Url;
                string Link = $"{ApplicationKeys.UrlWebSSA}/Acceso/ValidateSesionExternal?urlApp={Url.Authority}";
                return Redirect(Link);
                //return new RedirectResult(Link);
                //return JavaScript($"window.location = '{Link}'");
            }
            else if (opcionRedirect == "2")
            {
                //Session["TokenSGS"] = null;
                Session.Clear();
                Session.Abandon();
                //RedirectExternalUrlViewModel redirect = new RedirectExternalUrlViewModel();
                Uri Url = Request.Url;
                //redirect.urlApp = Url.Authority;
                //redirect.urlWebSSA = ApplicationKeys.UrlWebSSA;

                ViewBag.UrlApp = Url.Authority;
                ViewBag.UrlWebSSA = ApplicationKeys.UrlWebSSA;

                return View();
            }
            else
            {
                Session.Clear();
                Session.Abandon();
                Uri Url = Request.Url;
                string Link = $"{ApplicationKeys.UrlWebSSA}/Acceso/ValidateSesionExternal?urlApp={Url.Authority}";
                return JavaScript($"window.location = '{Link}'");
            }
        }

        [HttpPost]
        public ActionResult RedirectExternalUrl(RedirectExternalUrlViewModel redirect)
        {
            Uri Url = Request.Url;
            string Link = $"{ApplicationKeys.UrlWebSSA}/Acceso/ValidateSesionExternal?urlApp={Url.Authority}";
            return Redirect(Link);
        }

        // GET: Redirect
        public ActionResult Index(string Token, string IdPerfil)
        {
            if (!string.IsNullOrEmpty(Token))
            {
                Session["TokenSCC"] = Token;
                Session["IdPerfilSCC"] = IdPerfil;

                try
                {
                    //OBTENER LA INFORMACION DEL USUARIO (DESDE EL TOKEN)
                    SeguridadHelper helper = new SeguridadHelper();
                    //EloginResponse usuarioResponse = helper.ObtenerUsuario("", Token);
                    ResultadoObtenerModel<Usuario> usuarioResponse = helper.ObtenerSesionUsuario(Token, IdPerfil);

                    string strAction = "Index";
                    string strController = "Home";

                    //Session["UsuarioSCC"] = usuarioResponse;
                    if (usuarioResponse.Model.ListaOpcionesMenu.Any(m => m.NombreOpcion == "Cumplimiento"))
                    {
                        strAction = "Index";
                        strController = "Cumplimiento";
                    }
                    else
                    {
                        var opcionDefault = usuarioResponse.Model.ListaOpcionesMenu.FirstOrDefault();

                        if (opcionDefault.TipoOpcion == "1")
                        {
                            strController = opcionDefault.UrlOpcion.Split('/').LastOrDefault();
                        }                        
                    }
                    
                    return RedirectToAction(strAction, strController);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("UnauthorizedOperation", "Error", new { msjErrorExcepcion = ex.Message });
                }
            }

            return RedirectToAction("Index", "Home");
        }
    }
}