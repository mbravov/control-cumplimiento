﻿using Agrobanco.SCC.BLL;
using Agrobanco.SCC.DAL.Entidades;
using Agrobanco.SCC.Web.Models;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SCC.Web.Controllers
{
    public class SeguimientoController : BaseController
    {
        // GET: Seguimiento
        public ActionResult Index()
        {
            CargarDatosIniciales();

            List<SeguimientoModel> seguimientoModel = new List<SeguimientoModel>();

            ViewBag.listaArea = "0";
            ViewBag.fecVencimientoDesde = null;
            ViewBag.fecVencimientoHasta = null;
            ViewBag.estadoCumplimiento = "4";

            seguimientoModel = ObtenerListaSeguimientoModel(null, null, null, "4");

            return View(seguimientoModel);
        }

        private void CargarDatosIniciales()
        {
            ParametriaBLL parametria = new ParametriaBLL();
            List<ElementoBasico> areasFiltro = new List<ElementoBasico>();
            var areas = parametria.ObtenerListaArea();
            var listaEstadoCumplimiento = new List<ElementoBasico>();
            listaEstadoCumplimiento.Add(new ElementoBasico { Codigo = "1", Descripcion = "Dentro del Plazo" });
            listaEstadoCumplimiento.Add(new ElementoBasico { Codigo = "2", Descripcion = "Fuera de Plazo" });
            listaEstadoCumplimiento.Add(new ElementoBasico { Codigo = "3", Descripcion = "No enviado" });
            listaEstadoCumplimiento.Add(new ElementoBasico { Codigo = "4", Descripcion = "No informado" });


            foreach (var item in areas)
            {
                ElementoBasico elemento = new ElementoBasico();
                elemento.Codigo = item.CodigoElemento.ToString();
                elemento.Descripcion = item.Descripcion;

                areasFiltro.Add(elemento);
            }

            ViewBag.ListaAreaResponsable = areasFiltro;
            ViewBag.ListaEstadoCumplimiento = listaEstadoCumplimiento;
        }

        [HttpPost]
        public ActionResult Buscar(string hdListaArea, string fecVencimientoDesde, string fecVencimientoHasta, string estadoCumplimiento)
        {
            List<SeguimientoModel> listaSeguimiento = new List<SeguimientoModel>();            

            ViewBag.listaArea = hdListaArea;
            ViewBag.fecVencimientoDesde = fecVencimientoDesde;
            ViewBag.fecVencimientoHasta = fecVencimientoHasta;
            ViewBag.estadoCumplimiento = estadoCumplimiento;
            try
            {
                listaSeguimiento = ObtenerListaSeguimientoModel(hdListaArea, fecVencimientoDesde, fecVencimientoHasta, estadoCumplimiento);

                return PartialView("_ListaSeguimiento", listaSeguimiento);
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                throw new HttpException(500, ex.Message);
            }
            
        }

        private List<SeguimientoModel> ObtenerListaSeguimientoModel(string listaArea, string vencimientoDesde, string vencimientoHasta, string estadoCumplimiento)
        {
            List<SeguimientoModel> listaSeguimiento = new List<SeguimientoModel>();

            var reportes = ObtenerListaSeguimiento(listaArea, vencimientoDesde, vencimientoHasta, estadoCumplimiento);

            foreach (var item in reportes)
            {
                SeguimientoModel reporteModel = new SeguimientoModel();
                
                reporteModel.CodReporte = item.CodigoReporte;
                reporteModel.Nombre = item.NombreReporte;
                reporteModel.Descripcion = item.Descripcion;
                reporteModel.Entidad = item.Empresa;
                reporteModel.AreaResponsable = item.AreaResponsable;
                reporteModel.ResponsablePrincipal = item.ResponsablePrincipal;
                reporteModel.Frecuencia = item.Frecuencia;
                reporteModel.FechaVencimiento = string.Concat(item.ProximoVencimiento.Day.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Month.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Year.ToString());
                
                if (item.FechaVencimiento.HasValue)
                {
                    string dia = item.FechaVencimiento.Value.Day.ToString().PadLeft(2, '0');
                    string mes = item.FechaVencimiento.Value.Month.ToString().PadLeft(2, '0');
                    string anio = item.FechaVencimiento.Value.Year.ToString();

                    reporteModel.FechaVencimiento = string.Concat(dia, "/", mes, "/", anio);
                }

                reporteModel.FechaEnvio = string.Empty;

                if (item.FechaEnvio.HasValue)
                {
                    string dia = item.FechaEnvio.Value.Day.ToString().PadLeft(2, '0');
                    string mes = item.FechaEnvio.Value.Month.ToString().PadLeft(2, '0');
                    string anio = item.FechaEnvio.Value.Year.ToString();

                    reporteModel.FechaEnvio = string.Concat(dia, "/", mes, "/", anio);
                }

                reporteModel.FechaRegistro = string.Empty;

                if (item.FechaRegistro.HasValue)
                {
                    string dia = item.FechaRegistro.Value.Day.ToString().PadLeft(2, '0');
                    string mes = item.FechaRegistro.Value.Month.ToString().PadLeft(2, '0');
                    string anio = item.FechaRegistro.Value.Year.ToString();

                    reporteModel.FechaRegistro = string.Concat(dia, "/", mes, "/", anio);
                }

                reporteModel.EstadoCumplimiento = item.Cumplimiento.ToString();                

                listaSeguimiento.Add(reporteModel);
            }

            return listaSeguimiento;
        }

        private List<SeguimientoConsulta> ObtenerListaSeguimiento(string listaArea, string vencimientoDesde, string vencimientoHasta, string estadoCumplimiento)
        {
            SeguimientoBLL seguimientoBLL = new SeguimientoBLL();
            int? codEstadoCumplimiento = null;
            DateTime? dtVencimientoDesde = null;
            DateTime? dtVencimientoHasta = null;

            if (!string.IsNullOrEmpty(vencimientoDesde))
            {

                string[] arrFecha = vencimientoDesde.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoDesde = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (!string.IsNullOrEmpty(vencimientoHasta))
            {
                string[] arrFecha = vencimientoHasta.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoHasta = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (string.IsNullOrEmpty(listaArea))
            {
                listaArea = "0";
            }

            if (!string.IsNullOrEmpty(estadoCumplimiento))
            {
                codEstadoCumplimiento = Convert.ToInt32(estadoCumplimiento);
            }

            return seguimientoBLL.ObtenerListaSeguimiento(listaArea, dtVencimientoDesde, dtVencimientoHasta, codEstadoCumplimiento);
        }

        public void ExportarExcel(string listaArea, string fecVencimientoDesde, string fecVencimientoHasta, string estadoCumplimiento)
        {
            var reportes = ObtenerListaSeguimiento(listaArea, fecVencimientoDesde, fecVencimientoHasta, estadoCumplimiento);
            
            string fileNameReport = "ReporteSeguimiento_" + DateTime.Now.ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("HHmmss");

            GenerarExcelResponse(fileNameReport, "Hoja1", reportes, (ws, data) => {

                int rowIndex = 1;
                int lastColumn = 25;
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

                ws.Cell(rowIndex, 1).Value = "Nombre Reporte";
                ws.Cell(rowIndex, 2).Value = "Descripción de Reporte";
                ws.Cell(rowIndex, 3).Value = "Medio de Envío";
                ws.Cell(rowIndex, 4).Value = "Entidad";
                ws.Cell(rowIndex, 5).Value = "Área Responsable de Generación";
                ws.Cell(rowIndex, 6).Value = "Gerente Responsable de Generación";
                ws.Cell(rowIndex, 7).Value = "Responsable de Generación 1";
                ws.Cell(rowIndex, 8).Value = "Responsable de Generación 2";
                ws.Cell(rowIndex, 9).Value = "Área Responsable";
                ws.Cell(rowIndex, 10).Value = "Coordinador Responsable";
                ws.Cell(rowIndex, 11).Value = "Responsable Principal";
                ws.Cell(rowIndex, 12).Value = "Responsable Alterno";
                ws.Cell(rowIndex, 13).Value = "Gerente Responsable";
                ws.Cell(rowIndex, 14).Value = "Frecuencia del Reporte";
                ws.Cell(rowIndex, 15).Value = "Plazo (días)";
                ws.Cell(rowIndex, 16).Value = "Tipo días de plazo";
                ws.Cell(rowIndex, 17).Value = "Vencimiento";
                ws.Cell(rowIndex, 18).Value = "MES";
                ws.Cell(rowIndex, 19).Value = "AÑO";
                ws.Cell(rowIndex, 20).Value = "Fecha Envío";
                ws.Cell(rowIndex, 21).Value = "Fecha Registro";
                ws.Cell(rowIndex, 22).Value = "Motivo Incumplimiento";
                ws.Cell(rowIndex, 23).Value = "Comentarios";
                ws.Cell(rowIndex, 24).Value = "Cumplimiento";

                ws.Columns().AdjustToContents();

                foreach (var item in data)
                {
                    rowIndex++;

                    ws.Cell(rowIndex, 1).Value = item.NombreReporteE??item.NombreReporte;
                    ws.Cell(rowIndex, 2).Value = item.Descripcion??item.Descripcion;
                    ws.Cell(rowIndex, 3).Value = item.MedioEnvioDescripcion;
                    ws.Cell(rowIndex, 4).Value = item.Empresa;
                    ws.Cell(rowIndex, 5).Value = item.AreaGeneracion;

                    ws.Cell(rowIndex, 6).Value = item.GerenteGeneracion;
                    ws.Cell(rowIndex, 7).Value = item.ResponsableGeneracion1;
                    ws.Cell(rowIndex, 8).Value = item.ResponsableGeneracion2;
                    ws.Cell(rowIndex, 9).Value = item.AreaResponsable;
                    ws.Cell(rowIndex, 10).Value = item.Coordinador;
                    ws.Cell(rowIndex, 11).Value = item.ResponsablePrincipalE ?? item.ResponsablePrincipal;
                    ws.Cell(rowIndex, 12).Value = item.ResponsableAlterno;
                    ws.Cell(rowIndex, 13).Value = item.GerenteResponsable;
                    ws.Cell(rowIndex, 14).Value = item.Frecuencia;
                    ws.Cell(rowIndex, 15).Value = string.Concat(item.Plazo.ToString(), " días");

                    if (item.TipoDiasPlazo == 1)
                    {
                        ws.Cell(rowIndex, 16).Value = "Hábiles";
                    }

                    if (item.TipoDiasPlazo == 2)
                    {
                        ws.Cell(rowIndex, 16).Value = "Calendarios";
                    }

                    ws.Cell(rowIndex, 17).Value = item.FechaVencimiento ?? item.ProximoVencimiento;
                    ws.Cell(rowIndex, 18).Value = item.MesVencimiento;
                    ws.Cell(rowIndex, 19).Value = item.AnioVencimiento;
                    ws.Cell(rowIndex, 20).Value = item.FechaEnvio;
                    ws.Cell(rowIndex, 21).Value = item.FechaRegistro;
                    ws.Cell(rowIndex, 22).Value = item.MotivoIncumplimiento;
                    ws.Cell(rowIndex, 22).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 23).Value = item.Comentarios;
                    ws.Cell(rowIndex, 23).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 24).Value = item.DescripcionCumplimiento;
                    ws.Cell(rowIndex, 24).Style.Font.Bold = true;

                    switch (item.Cumplimiento)
                    {
                        case 1:
                            ws.Cell(rowIndex, 24).Style.Font.FontColor = XLColor.Green;
                            break;
                        case 2:
                            ws.Cell(rowIndex, 24).Style.Font.FontColor = XLColor.Red;
                            break;
                        case 3:
                            ws.Cell(rowIndex, 24).Style.Font.FontColor = XLColor.Blue;
                            break;
                        case 4:
                            ws.Cell(rowIndex, 24).Style.Font.FontColor = XLColor.Black;
                            break;
                        default:
                            ws.Cell(rowIndex, 24).Style.Font.FontColor = XLColor.Black;
                            break;
                    }



                }


            });

        }

        
    }
}