﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Agrobanco.SCC.BLL;
using Agrobanco.SCC.DAL.Entidades;
using Agrobanco.SCC.Web.Helpers;
using Agrobanco.SCC.Web.Models;
using PagedList;

namespace Agrobanco.SCC.Web.Controllers
{
    public class ConfiguracionController : BaseController
    {
        // GET: Configuracion
        public ActionResult Index()
        {                       
            ViewBag.VencimientoDesde = string.Empty;
            ViewBag.VencimientoHasta = string.Empty;

            CargarDatosIniciales();
            var onePageListaReporte = ObtenerListaReporteBusqueda(1, 10, "0", string.Empty, string.Empty);

            return View(onePageListaReporte);
        }

        [HttpPost]
        public ActionResult Buscar(int? page, string hdListaArea, string fecVencimientoDesde, string fecVencimientoHasta)
        {
            ViewBag.hdListaArea = hdListaArea;
            ViewBag.fecVencimientoDesde = fecVencimientoDesde;
            ViewBag.fecVencimientoHasta = fecVencimientoHasta;
            try
            {
                var onePageListaReporte = ObtenerListaReporteBusqueda(page, 10, hdListaArea, fecVencimientoDesde, fecVencimientoHasta);

                return PartialView("_Lista", onePageListaReporte);
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }            
        }

        [HttpPost]
        public JsonResult RegistrarReporte(Reporte reporte)
        {
            ConfiguracionBLL configuracion = new ConfiguracionBLL();
            RespuestaOperacion response = new RespuestaOperacion();

            try
            {
                 var usuarioSesion = (ResultadoObtenerModel<Helpers.Usuario>)Session["UsuarioSCC"];

                reporte.UsuCreacion = usuarioSesion.Model.UsuarioWeb;
                reporte.UsuActualizacion = usuarioSesion.Model.UsuarioWeb;

                bool validacionExitosa = true;

                if (!string.IsNullOrEmpty(reporte.NombreReporte))
                {
                    validacionExitosa = configuracion.ValidarNombreReporte(reporte.CodigoReporte, reporte.NombreReporte);
                }                

                if (!validacionExitosa)
                {
                    response = new RespuestaOperacion { Resultado = 0, Mensaje = "El nombre del reporte ingresado, ya se encuentra registrado" };

                    return Json(response);
                }

                int resultadoRegistro = configuracion.RegistrarReporte(reporte);

                response = new RespuestaOperacion { Resultado = 1, Mensaje = "OK" };
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                response = new RespuestaOperacion { Resultado = 0, Mensaje = "Ha ocurrido un error en la aplicación" };
            }

            return Json(response);
        }

        [HttpGet]
        public JsonResult ObtenerReporte(int codigoReporte)
        {
            ConfiguracionBLL configuracion = new ConfiguracionBLL();
            RespuestaOperacion response = new RespuestaOperacion();

            try
            {                
                List<Reporte> listaReporte = configuracion.ObtenerListaReporte(codigoReporte);

                var reporte = listaReporte.FirstOrDefault();

                response = new RespuestaOperacion { Resultado = 1, Mensaje = "OK", Data = reporte};
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                response = new RespuestaOperacion { Resultado = 0, Mensaje = "Ha ocurrido un error en la aplicación" };
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //private IPagedList<ReporteModel> ObtenerListaReporte(int? page, int pageSize, List<int> listaArea, string vencimientoDesde, string vencimientoHasta)
        //{
        //    List<ReporteModel> listaReporte = new List<ReporteModel>();
        //    int pageNumber = page ?? 1;
            
        //    ConfiguracionBLL configuracion = new ConfiguracionBLL();

        //    DateTime? dtVencimientoDesde = string.IsNullOrEmpty(vencimientoDesde) ? (DateTime?)null : Convert.ToDateTime(vencimientoDesde);
        //    DateTime? dtVencimientoHasta = string.IsNullOrEmpty(vencimientoHasta) ? (DateTime?)null : Convert.ToDateTime(vencimientoHasta);

        //    var reportes = configuracion.ObtenerListaReporte(listaArea, dtVencimientoDesde, dtVencimientoHasta);
        //    //var reportes = configuracion.ObtenerListaReporteBusqueda(listaArea, dtVencimientoDesde, dtVencimientoHasta);

        //    foreach (var item in reportes)
        //    {
        //        ReporteModel reporteModel = new ReporteModel();

        //        reporteModel.CodReporte = item.CodigoReporte;
        //        reporteModel.Nombre = item.NombreReporte;
        //        reporteModel.Descripcion = item.Descripcion;
        //        reporteModel.Entidad = item.Empresa;
        //        reporteModel.AreaResponsable = item.AreaResponsable;
        //        reporteModel.Frecuencia = item.Frecuencia;
        //        reporteModel.Plazo = string.Concat(item.Plazo, " días");
        //        reporteModel.ProximoVencimiento = string.Concat(item.ProximoVencimiento.Day.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Month.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Year.ToString());
        //        reporteModel.ResponsablePrincipal = item.ResponsablePrincipal;

        //        listaReporte.Add(reporteModel);
        //    }


        //    var listaResultado = listaReporte.ToPagedList(pageNumber, pageSize);

        //    return listaResultado;
        //}

        private IPagedList<ReporteModel> ObtenerListaReporteBusqueda(int? page, int pageSize, string listaArea, string vencimientoDesde, string vencimientoHasta)
        {
            List<ReporteModel> listaReporte = new List<ReporteModel>();
            int pageNumber = page ?? 1;

            ConfiguracionBLL configuracion = new ConfiguracionBLL();

            DateTime? dtVencimientoDesde = null;
            DateTime? dtVencimientoHasta = null;

            if (!string.IsNullOrEmpty(vencimientoDesde))
            {

                string[] arrFecha = vencimientoDesde.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoDesde = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (!string.IsNullOrEmpty(vencimientoHasta))
            {
                string[] arrFecha = vencimientoHasta.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoHasta = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (string.IsNullOrEmpty(listaArea))
            {
                listaArea = "0";
            }           

            //var reportes = configuracion.ObtenerListaReporte(listaArea, dtVencimientoDesde, dtVencimientoHasta);
            var reportes = configuracion.ObtenerListaReporteBusqueda(listaArea, dtVencimientoDesde, dtVencimientoHasta);

            foreach (var item in reportes)
            {
                ReporteModel reporteModel = new ReporteModel();

                reporteModel.CodReporte = item.CodigoReporte;
                reporteModel.Nombre = item.NombreReporte;
                reporteModel.Descripcion = item.Descripcion;
                reporteModel.Entidad = item.Empresa;
                reporteModel.AreaResponsable = item.AreaResponsable;
                reporteModel.Frecuencia = item.Frecuencia;
                reporteModel.Plazo = string.Concat(item.Plazo, " días");
                reporteModel.ProximoVencimiento = string.Concat(item.ProximoVencimiento.Day.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Month.ToString().PadLeft(2, '0'), "/", item.ProximoVencimiento.Year.ToString());
                reporteModel.ResponsablePrincipal = item.ResponsablePrincipal;

                listaReporte.Add(reporteModel);
            }

            var listaResultado = listaReporte.ToPagedList(pageNumber, pageSize);

            return listaResultado;
        }

        

        private void CargarDatosIniciales()
        {
            ParametriaBLL parametria = new ParametriaBLL();
            List<ElementoBasico> areasFiltro = new List<ElementoBasico>();
            var areas = parametria.ObtenerListaArea();
            //areasFiltro.Add(new ElementoBasico { Codigo = string.Empty, Descripcion = "Todos", Selected = true, Disabled = true });

            foreach (var item in areas)
            {
                ElementoBasico elemento = new ElementoBasico();
                elemento.Codigo = item.CodigoElemento.ToString();
                elemento.Descripcion = item.Descripcion;
                
                areasFiltro.Add(elemento);
            }

            ViewBag.ListaAreaResponsable = areasFiltro;
            ViewBag.ListaArea = parametria.ObtenerListaArea();
            ViewBag.ListaEmpresa = parametria.ObtenerListaEmpresa();
            ViewBag.ListaFrecuencia = parametria.ObtenerListaFrecuencia();
            ViewBag.ListaMedioEnvio = parametria.ObtenerListaMedioEnvio();
            ViewBag.ListaUsuario = parametria.ObtenerListaUsuario();
        }
    }
}