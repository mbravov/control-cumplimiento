﻿using Agrobanco.SCC.BLL;
using Agrobanco.SCC.DAL;
using Agrobanco.SCC.DAL.Entidades;
using Agrobanco.SCC.Web.Helpers;
using Agrobanco.SCC.Web.Models;
using ClosedXML.Excel;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SCC.Web.Controllers
{
    public class CumplimientoController : BaseController
    {
        // GET: Cumplimiento
        public ActionResult Index()
        {
            ViewBag.VencimientoDesde = string.Empty;
            ViewBag.VencimientoHasta = string.Empty;

            CargarDatosIniciales();

            var model = ObtenerListaEnvioBusquedaModel(1, 10, null, null, null, null);

            return View(model);
        }


        private void CargarDatosIniciales()
        {
            ParametriaBLL parametria = new ParametriaBLL();
            ConfiguracionBLL configuracion = new ConfiguracionBLL();
            List<ElementoBasico> areasFiltro = new List<ElementoBasico>();
            var areas = parametria.ObtenerListaArea();
            var usuarios = parametria.ObtenerListaUsuario();
            List<Reporte> reportesFiltrados = new List<Reporte>();
            foreach (var item in areas)
            {
                ElementoBasico elemento = new ElementoBasico();
                elemento.Codigo = item.CodigoElemento.ToString();
                elemento.Descripcion = item.Descripcion;

                areasFiltro.Add(elemento);
            }

            reportesFiltrados = configuracion.ObtenerListaReporte();
            var usuarioSesion = (ResultadoObtenerModel<Helpers.Usuario>)Session["UsuarioSCC"];
            var usuario = usuarios.Where(u => u.WebUser == usuarioSesion.Model.UsuarioWeb).FirstOrDefault();

            var usersCumplimiento = parametria.ObtenerListaCorreosCumplimiento();

            if (usuario == null)
            {
                return;
            }

            if (!usersCumplimiento.Where(uc => uc.Valor == usuario.WebUser).Any())
            {
                reportesFiltrados = reportesFiltrados.Where(r => r.CodResponsablePrincipal == usuario.CodUsuario ||
                                                                r.CodResponsableAlterno == usuario.CodUsuario ||
                                                                r.CodGerenteResponsable == usuario.CodUsuario ||
                                                                r.CodCoordinador == usuario.CodUsuario).ToList();
            }

            ViewBag.ListaReportes = reportesFiltrados;
            ViewBag.ListaAreaResponsable = areasFiltro;
            ViewBag.ListaEntidad = parametria.ObtenerListaEmpresa();
            ViewBag.ListaFrecuencia = parametria.ObtenerListaFrecuencia();
            ViewBag.ListaUsuario = usuarios;
            ViewBag.ListaMotivo = parametria.ObtenerListaMotivo();
        }

        private IPagedList<EnvioModel> ObtenerListaEnvioBusquedaModel(int? page, int pageSize, string listaEntidad, string listaFrecuencia, string vencimientoDesde, string vencimientoHasta)
        {
            List<EnvioModel> envioModel = new List<EnvioModel>();
            int pageNumber = page ?? 1;
            
            var listaEnvio = ObtenerListaEnvioBusqueda(listaEntidad, listaFrecuencia, vencimientoDesde, vencimientoHasta);

            foreach (var item in listaEnvio)
            {
                EnvioModel envioM = new EnvioModel();

                envioM.CodEnvio = item.CodEnvio;
                envioM.CodReporte = item.CodReporte;
                envioM.NombreReporte = item.NombreReporte;
                envioM.Entidad = item.Entidad;
                envioM.AreaResponsable = item.AreaResponsable;
                envioM.ResponsablePrincipal = item.ResponsablePrincipal;
                envioM.Frecuencia = item.Frecuencia;
                envioM.FechaVencimiento = item.FechaVencimiento.ToString();
                envioM.FechaEnvio = item.FechaEnvio.ToString();

                envioModel.Add(envioM);
            }

            var listaResultado = envioModel.ToPagedList(pageNumber, pageSize);

            return listaResultado;
        }

        private List<Envio> ObtenerListaEnvioBusqueda(string listaEntidad, string listaFrecuencia, string vencimientoDesde, string vencimientoHasta)
        {
            CumplimientoBLL cumplimiento = new CumplimientoBLL();
            ParametriaBLL parametria = new ParametriaBLL();

            DateTime? dtVencimientoDesde = null;
            DateTime? dtVencimientoHasta = null;

            if (!string.IsNullOrEmpty(vencimientoDesde))
            {

                string[] arrFecha = vencimientoDesde.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoDesde = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (!string.IsNullOrEmpty(vencimientoHasta))
            {
                string[] arrFecha = vencimientoHasta.Split('/');
                string dia = arrFecha[0];
                string mes = arrFecha[1];
                string anio = arrFecha[2];

                dtVencimientoHasta = new DateTime(Convert.ToInt32(anio), Convert.ToInt32(mes), Convert.ToInt32(dia));
            }

            if (string.IsNullOrEmpty(listaEntidad))
            {
                listaEntidad = "0";
            }

            if (string.IsNullOrEmpty(listaFrecuencia))
            {
                listaFrecuencia = "0";
            }

            var listaEnvio = cumplimiento.ObtenerListaEnvioBusqueda(listaEntidad, listaFrecuencia, dtVencimientoDesde, dtVencimientoHasta);
            var usuarios = parametria.ObtenerListaUsuario();
            var usuarioSesion = (ResultadoObtenerModel<Helpers.Usuario>)Session["UsuarioSCC"];
            var usuario = usuarios.Where(u => u.WebUser == usuarioSesion.Model.UsuarioWeb).FirstOrDefault();

            var usersCumplimiento = parametria.ObtenerListaCorreosCumplimiento();

            if (usuario == null)
            {
                return new List<Envio>();
            }

            if (!usersCumplimiento.Where(uc => uc.Valor == usuario.WebUser).Any())
            {
                listaEnvio = listaEnvio.Where(e => e.CodResponsablePrincipal == usuario.CodUsuario ||
                                                                e.CodResponsableAlterno == usuario.CodUsuario ||
                                                                e.CodGerenteResponsable == usuario.CodUsuario ||
                                                                e.CodCoordinador == usuario.CodUsuario).ToList();
            }

            return listaEnvio;
        }

        [HttpPost]
        public ActionResult Buscar(int? page, string hdListaEntidad, string hdListaFrecuencia, string fecVencimientoDesde, string fecVencimientoHasta)
        {
            ViewBag.hdListaEntidad = hdListaEntidad;
            ViewBag.hdListaFrecuencia = hdListaFrecuencia;
            ViewBag.fecVencimientoDesde = fecVencimientoDesde;
            ViewBag.fecVencimientoHasta = fecVencimientoHasta;
            try
            {
                var onePageListaEnvio = ObtenerListaEnvioBusquedaModel(page, 10, hdListaEntidad, hdListaFrecuencia, fecVencimientoDesde, fecVencimientoHasta);

                return PartialView("_ListaCumplimiento", onePageListaEnvio);
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                throw new HttpException(500, ex.Message);
            }            
        }

        [HttpPost]
        public ActionResult RegistrarEnvio(Envio envio)
        {
            CumplimientoBLL cumplimiento = new CumplimientoBLL();
            RespuestaOperacion response = new RespuestaOperacion();

            try
            {
                System.IO.Stream fileContent = null;

                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    int fileSize = file.ContentLength;
                    string fileName = file.FileName;
                    string mimeType = file.ContentType;
                    fileContent = file.InputStream;                    
                }

                var usuarioSesion = (ResultadoObtenerModel<Helpers.Usuario>)Session["UsuarioSCC"];
                envio.UsuarioCreacion = usuarioSesion.Model.UsuarioWeb;

                DateTime nuevaFechaVencimiento = cumplimiento.RegistrarEnvio(envio, fileContent);
                string strFechaVencimiento = string.Concat(nuevaFechaVencimiento.Day.ToString().PadLeft(2, '0'), "/", nuevaFechaVencimiento.Month.ToString().PadLeft(2, '0'), "/", nuevaFechaVencimiento.Year.ToString());

                response = new RespuestaOperacion { Resultado = 1, Mensaje = "OK", Data = new { nuevaFechaVencimiento, strFechaVencimiento } };
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                response = new RespuestaOperacion { Resultado = 0, Mensaje = "Ha ocurrido un error en la aplicación" };
            }

            return Json(response);
        }

        [HttpGet]
        public JsonResult ObtenerEnvio(int codEnvio)
        {
            CumplimientoBLL cumplimiento = new CumplimientoBLL();
            RespuestaOperacion response = new RespuestaOperacion();

            try
            {
                List<Envio> listaEnvio = cumplimiento.ObtenerListaEnvio(codEnvio);

                var envio = listaEnvio.FirstOrDefault();
                bool dentroDelPlazo = false;

                if (envio.CodEstadoEnvio == (int)Enums.EstadoEnvio.Enviado)
                {
                    dentroDelPlazo = (envio.FechaEnvio > envio.FechaVencimiento) ? false : true;
                }

                response = new RespuestaOperacion { Resultado = 1, Mensaje = "OK", Data = new { envio, dentroDelPlazo } };
            }
            catch (Exception ex)
            {
                GenerarLog(ex.Message);
                response = new RespuestaOperacion { Resultado = 0, Mensaje = "Ha ocurrido un error en la aplicación" };
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportarSustento(int codigoSustento)
        {
            CumplimientoBLL cumplimiento = new CumplimientoBLL();
            string filename = string.Empty;
            string extension = string.Empty;            

            string file = cumplimiento.ObtenerSustento(codigoSustento, out filename, out extension);

            byte[] imageBytes = Convert.FromBase64String(file);

            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);

            return File(ms2.GetBuffer(), "application/pdf", filename);
        }

        public void ExportarExcel(string listaEntidad, string listaArea, string fecVencimientoDesde, string fecVencimientoHasta)
        {
            var reportes = ObtenerListaEnvioBusqueda(listaEntidad, listaArea, fecVencimientoDesde, fecVencimientoHasta);

            string fileNameReport = "ReporteCumplimiento_" + DateTime.Now.ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("HHmmss");

            GenerarExcelResponse(fileNameReport, "Hoja1", reportes, (ws, data) => {

                int rowIndex = 1;
                int lastColumn = 13;
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                ws.Range(rowIndex, 1, rowIndex, lastColumn).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

                ws.Cell(rowIndex, 1).Value = "Nombre Reporte";                
                ws.Cell(rowIndex, 2).Value = "Entidad";                
                ws.Cell(rowIndex, 3).Value = "Área Responsable";
                ws.Cell(rowIndex, 4).Value = "Responsable Principal";
                ws.Cell(rowIndex, 5).Value = "Responsable Alterno";                
                ws.Cell(rowIndex, 6).Value = "Frecuencia del Reporte";                
                ws.Cell(rowIndex, 7).Value = "Vencimiento";                
                ws.Cell(rowIndex, 8).Value = "Fecha Envío";
                ws.Cell(rowIndex, 9).Value = "Fecha Registro";
                ws.Cell(rowIndex, 10).Value = "Motivo Incumplimiento";
                ws.Cell(rowIndex, 11).Value = "Comentarios";                

                ws.Columns().AdjustToContents();

                foreach (var item in data)
                {
                    rowIndex++;

                    ws.Cell(rowIndex, 1).Value = item.NombreReporte;                                        
                    ws.Cell(rowIndex, 2).Value = item.Entidad;                    
                    ws.Cell(rowIndex, 3).Value = item.AreaResponsable;                    
                    ws.Cell(rowIndex, 4).Value = item.ResponsablePrincipal;
                    ws.Cell(rowIndex, 5).Value = item.ResponsableAlterno;                    
                    ws.Cell(rowIndex, 6).Value = item.Frecuencia;                   
                    ws.Cell(rowIndex, 7).Value = item.FechaVencimiento;                    
                    ws.Cell(rowIndex, 8).Value = item.FechaEnvio;
                    ws.Cell(rowIndex, 9).Value = item.FechaRegistro;
                    ws.Cell(rowIndex, 10).Value = item.MotivoIncumplimiento;
                    ws.Cell(rowIndex, 10).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 11).Value = item.Comentarios;
                    ws.Cell(rowIndex, 11).Style.Alignment.WrapText = true;

                }


            });

        }
    }
}