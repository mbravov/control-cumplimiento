USE [DBAgrCumplimiento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovEnvio]') AND type in (N'U'))
DROP TABLE [dbo].[MovEnvio]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeReporteVencimiento]') AND type in (N'U'))
DROP TABLE [dbo].[MaeReporteVencimiento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeReporte]') AND type in (N'U'))
DROP TABLE [dbo].[MaeReporte]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParParametro]') AND type in (N'U'))
DROP TABLE [dbo].[ParParametro]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeEmpresa]') AND type in (N'U'))
DROP TABLE [dbo].[MaeEmpresa]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeUsuario]') AND type in (N'U'))
DROP TABLE [dbo].[MaeUsuario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeArea]') AND type in (N'U'))
DROP TABLE [dbo].[MaeArea]
GO

CREATE TABLE MaeUsuario
(
	iCodUsuario INT NOT NULL IDENTITY(1, 1),
	vNombres VARCHAR(400) NOT NULL,
	vEmail VARCHAR(200) NOT NULL,
	vWEBUSER VARCHAR(50) NOT NULL,
	vCargo VARCHAR(200),
	iCodArea INT NULL,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MaeUsuario] WITH CHECK ADD CONSTRAINT [PK_MaeUsuario_CodUsuario] PRIMARY KEY([iCodUsuario])
GO

CREATE TABLE MaeArea
(
	iCodArea INT NOT NULL IDENTITY(1, 1),
	vDescripcion VARCHAR(200) NOT NULL,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MaeArea] WITH CHECK ADD CONSTRAINT [PK_MaeArea_CodArea] PRIMARY KEY([iCodArea])
GO

CREATE TABLE ParParametro
(
	iCodParametro INT NOT NULL IDENTITY(1, 1),
	iCodGrupo INT NOT NULL,	
	iCodElemento INT NOT NULL,
	vDescripcion VARCHAR(200),
	vValor VARCHAR(200),
	iOrden INT,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

CREATE TABLE MaeEmpresa
(
	iCodEmpresa INT NOT NULL IDENTITY(1, 1),
	vDescripcion VARCHAR(200) NOT NULL,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MaeEmpresa] WITH CHECK ADD CONSTRAINT [PK_MaeEmpresa_CodEmpresa] PRIMARY KEY([iCodEmpresa])
GO

CREATE TABLE [dbo].[MaeReporteVencimiento]
(
	iCodVencimiento INT NOT NULL IDENTITY(1, 1),
	iCodReporte INT,
	iEstadoVencimiento BIT,
	iRecordatorio1 BIT NOT NULL,
	iRecordatorio2 BIT NOT NULL,
	iAlertaVencimiento BIT NOT NULL,
	iAlertaIncumplimiento BIT NOT NULL,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MaeReporteVencimiento] WITH CHECK ADD CONSTRAINT [PK_MaeReporteVencimiento_CodVencimiento] PRIMARY KEY([iCodVencimiento])
GO

CREATE TABLE MaeReporte
(
	iCodReporte INT NOT NULL IDENTITY(1, 1),
	vNombre VARCHAR(70) NOT NULL,
	vDescripcion VARCHAR(400) NOT NULL,
	iCodEmpresa INT NOT NULL,
	iMedioEnvio INT,
	iCodAreaGen INT NOT NULL,
	iCodGerenteGen INT NOT NULL,
	iCodResponsableGen1 INT,
	iCodResponsableGen2 INT,
	iCodAreaResponsable INT NOT NULL,
	iCodGerenteResponsable INT NOT NULL,
	iCodCoordinador INT NOT NULL,
	iCodResponsablePrincipal INT NOT NULL,
	iCodResponsableAlterno INT,
	iCodFrecuencia INT NOT NULL,
	dProximoVencimiento DATETIME,
	iCodTipoCalculo INT NOT NULL,
	iPlazo INT,
	iTipoDiasPlazo INT,
	iDiasRecordatorio1 INT,
	iDiasRecordatorio2 INT,
	iEstado INT NOT NULL,
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [PK_MaeReporte_CodReporte] PRIMARY KEY([iCodReporte])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeEmpresa] FOREIGN KEY([iCodEmpresa])
REFERENCES [dbo].[MaeEmpresa] ([iCodEmpresa])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeAreaGen] FOREIGN KEY([iCodAreaGen])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioGerenteGen] FOREIGN KEY([iCodGerenteGen])
REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
GO

--ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioRespGen1] FOREIGN KEY([iCodResponsableGen1])
--REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
--GO

--ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioRespGen2] FOREIGN KEY([iCodResponsableGen2])
--REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
--GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeAreaResponsable] FOREIGN KEY([iCodAreaResponsable])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioGerenteResp] FOREIGN KEY([iCodGerenteResponsable])
REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioCoordinador] FOREIGN KEY([iCodCoordinador])
REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
GO

ALTER TABLE [dbo].[MaeReporte] WITH CHECK ADD CONSTRAINT [FK_MaeReporte_MaeUsuarioRespPrincipal] FOREIGN KEY([iCodResponsablePrincipal])
REFERENCES [dbo].[MaeUsuario] ([iCodUsuario])
GO

CREATE TABLE MovEnvio
(
	iCodEnvio INT NOT NULL IDENTITY(1, 1),
	iCodReporte INT NOT NULL,
	vNombreReporte VARCHAR(50),
	vEmpresa VARCHAR(300),
	vAreaResponsable VARCHAR(200),
	vGerenteResponsable VARCHAR(400),
	iCodResponsablePrincipal INT,
	vResponsablePrincipal VARCHAR(400),
	iCodResponsableAlterno INT,
	vResponsableAlterno VARCHAR(400),
	iEstadoEnvio INT NOT NULL,
	iCodVencimiento INT,
	dFecVencimiento DATETIME NOT NULL,
	dFecEnvio DATETIME NULL,
	dFecRegistro DATETIME NOT NULL,
	vMotivoIncumplimiento VARCHAR(200),
	vComentarios VARCHAR(500),
	iCodSustento INT NOT NULL,
	vNombreArchivo VARCHAR(100),
	vUsuCreacion VARCHAR(100)NOT NULL,
	dFecCreacion DATETIME NOT NULL,
	vUsuActualizacion VARCHAR(100) NULL,
	dFecActualizacion DATETIME NULL
)
GO

ALTER TABLE [dbo].[MovEnvio] WITH CHECK ADD CONSTRAINT [FK_MovEnvio_CodEnvio] PRIMARY KEY([iCodEnvio])
GO

ALTER TABLE [dbo].[MovEnvio] WITH CHECK ADD CONSTRAINT [FK_MovEnvio_MaeReporte] FOREIGN KEY([iCodReporte])
REFERENCES [dbo].[MaeReporte] ([iCodReporte])
GO