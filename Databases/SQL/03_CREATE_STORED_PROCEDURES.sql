USE [DBAgrCumplimiento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Split]
GO

CREATE FUNCTION [dbo].[Split](@String varchar(150), @Delimiter char(1))
RETURNS @Results table (word varchar(50))
AS
BEGIN
--select * from dbo.Split('danielsa asds asdsd asdsa d|vargas|yalico|451420751|carlos|cabos','|')a
DECLARE @INDEX INT
DECLARE @SLICE varchar(200)
-- Asignar 1 a la variable que utilizaremos en el loop para no iniciar en 0.
SELECT @INDEX = 1

WHILE @INDEX !=0
BEGIN
-- Obtenemos el �ndice de la primera ocurrencia del split de caracteres.
SELECT @INDEX = CHARINDEX(@Delimiter,@STRING)
-- Ahora ponemos todo a la izquierda de el slice de la variable.
IF @INDEX != 0
SELECT @SLICE = LEFT(@STRING,@INDEX - 1)
ELSE
SELECT @SLICE = @STRING

insert into @Results(word) values(@SLICE)
SELECT @STRING = RIGHT(@STRING,LEN(@STRING) - @INDEX)

-- Salimos del loop si terminamos la b�squeda
IF LEN(@STRING) = 0 BREAK
END

RETURN

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaReporte]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaReporte]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaReporte]
(
	@CodReporte INT NULL
)
AS
BEGIN
	
	SELECT
		R.iCodReporte,
		R.vNombre,
		R.vDescripcion,
		R.iCodEmpresa,
		E.vDescripcion AS Empresa,
		R.iMedioEnvio,
		PARME.vDescripcion AS MedioEnvio,
		R.iCodAreaGen,
		AGEN.vDescripcion AS AreaGeneracion,
		R.iCodGerenteGen,
		GERGEN.vNombres AS GerenteGeneracion,
		R.iCodResponsableGen1,
		RESP1GEN.vNombres AS ResponsableGeneracion1,
		R.iCodResponsableGen2,
		RESP2GEN.vNombres AS ResponsableGeneracion2,
		R.iCodAreaResponsable,
		ARES.vDescripcion AS AreaResponsable,
		R.iCodGerenteResponsable,
		GERRESP.vNombres AS GerenteResponsable,
		R.iCodCoordinador,
		COORESP.vNombres AS Coordinador,
		R.iCodResponsablePrincipal,
		RESPP.vNombres AS ResponsablePrincipal,
		R.iCodResponsableAlterno,
		RESPA.vNombres AS ResponsableAlterno,
		R.iCodFrecuencia,
		PARF.vDescripcion AS Frecuencia,
		R.dProximoVencimiento,
		R.iCodTipoCalculo,
		R.iPlazo,
		R.iTipoDiasPlazo,
		R.iDiasRecordatorio1,
		R.iDiasRecordatorio2,
		R.iEstado		
	FROM MaeReporte R
		INNER JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		INNER JOIN MaeArea AGEN ON R.iCodAreaGen = AGEN.iCodArea
		INNER JOIN MaeUsuario GERGEN ON R.iCodGerenteGen = GERGEN.iCodUsuario
		INNER JOIN MaeUsuario RESP1GEN ON R.iCodResponsableGen1 = RESP1GEN.iCodUsuario
		INNER JOIN MaeUsuario RESP2GEN ON R.iCodResponsableGen2 = RESP2GEN.iCodUsuario
		INNER JOIN MaeArea ARES ON R.iCodAreaResponsable = ARES.iCodArea
		INNER JOIN MaeUsuario GERRESP ON R.iCodGerenteResponsable = GERRESP.iCodUsuario
		INNER JOIN MaeUsuario COORESP ON R.iCodCoordinador = COORESP.iCodUsuario
		INNER JOIN MaeUsuario RESPP ON R.iCodResponsablePrincipal = RESPP.iCodUsuario
		LEFT JOIN MaeUsuario RESPA ON R.iCodResponsableAlterno = RESPA.iCodUsuario
		LEFT JOIN ParParametro PARME ON (PARME.iCodGrupo = 2 AND R.iMedioEnvio = PARME.iCodElemento)
		LEFT JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
	WHERE (@CodReporte IS NOT NULL AND R.iCodReporte = @CodReporte) OR (@CodReporte IS NULL)	
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaReporteVencimiento]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaReporteVencimiento]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaReporteVencimiento]
(
	@CodReporte INT NULL
)
AS
BEGIN
	
	SELECT
		R.iCodReporte,
		R.vNombre,
		R.vDescripcion,
		R.iCodEmpresa,
		E.vDescripcion AS Empresa,
		R.iMedioEnvio,
		PARME.vDescripcion AS MedioEnvio,
		R.iCodAreaGen,
		AGEN.vDescripcion AS AreaGeneracion,
		R.iCodGerenteGen,
		GERGEN.vNombres AS GerenteGeneracion,
		R.iCodResponsableGen1,
		RESP1GEN.vNombres AS ResponsableGeneracion1,
		RESP1GEN.vEmail AS EmailRespGeneracion1,
		R.iCodResponsableGen2,
		RESP2GEN.vNombres AS ResponsableGeneracion2,
		R.iCodAreaResponsable,
		ARES.vDescripcion AS AreaResponsable,
		R.iCodGerenteResponsable,
		GERRESP.vNombres AS GerenteResponsable,
		GERRESP.vEmail AS EmailGerenteResponsable,
		R.iCodCoordinador,
		COORESP.vNombres AS Coordinador,
		COORESP.vEmail AS EmailCoordinador,
		R.iCodResponsablePrincipal,
		RESPP.vNombres AS ResponsablePrincipal,
		RESPP.vEmail AS EmailRespPrincipal,
		R.iCodResponsableAlterno,
		RESPA.vNombres AS ResponsableAlterno,
		RESPA.vEmail AS EmailRespAlterno,
		R.iCodFrecuencia,
		PARF.vDescripcion AS Frecuencia,
		R.dProximoVencimiento,
		R.iCodTipoCalculo,
		R.iPlazo,
		R.iTipoDiasPlazo,
		R.iDiasRecordatorio1,
		[dbo].[FN_ObtenerFechaRecordatorio](R.iDiasRecordatorio1, R.dProximoVencimiento) AS FechaRecordatorio1,
		R.iDiasRecordatorio2,
		[dbo].[FN_ObtenerFechaRecordatorio](R.iDiasRecordatorio2, R.dProximoVencimiento) AS FechaRecordatorio2,
		R.iEstado,
		RV.iRecordatorio1,
		RV.iRecordatorio2,
		RV.iAlertaVencimiento,
		RV.iAlertaIncumplimiento
	FROM MaeReporte R
		INNER JOIN MaeReporteVencimiento RV ON R.iCodReporte = RV.iCodReporte AND RV.iEstadoVencimiento = 1
		INNER JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		INNER JOIN MaeArea AGEN ON R.iCodAreaGen = AGEN.iCodArea
		INNER JOIN MaeUsuario GERGEN ON R.iCodGerenteGen = GERGEN.iCodUsuario
		INNER JOIN MaeUsuario RESP1GEN ON R.iCodResponsableGen1 = RESP1GEN.iCodUsuario
		INNER JOIN MaeUsuario RESP2GEN ON R.iCodResponsableGen2 = RESP2GEN.iCodUsuario
		INNER JOIN MaeArea ARES ON R.iCodAreaResponsable = ARES.iCodArea
		INNER JOIN MaeUsuario GERRESP ON R.iCodGerenteResponsable = GERRESP.iCodUsuario
		INNER JOIN MaeUsuario COORESP ON R.iCodCoordinador = COORESP.iCodUsuario
		INNER JOIN MaeUsuario RESPP ON R.iCodResponsablePrincipal = RESPP.iCodUsuario
		LEFT JOIN MaeUsuario RESPA ON R.iCodResponsableAlterno = RESPA.iCodUsuario
		LEFT JOIN ParParametro PARME ON (PARME.iCodGrupo = 2 AND R.iMedioEnvio = PARME.iCodElemento)
		LEFT JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
	WHERE (@CodReporte IS NOT NULL AND R.iCodReporte = @CodReporte) OR (@CodReporte IS NULL)	
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaReporteBusqueda]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaReporteBusqueda]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaReporteBusqueda]
(	
	@Areas VARCHAR(50),	
	@VctoDesde DATETIME NULL,
	@VctoHasta DATETIME NULL
)
AS
BEGIN
	
	SELECT
		R.iCodReporte,
		R.vNombre,
		R.vDescripcion,
		R.iCodEmpresa,
		E.vDescripcion AS Empresa,
		R.iMedioEnvio,
		PARME.vDescripcion AS MedioEnvio,
		R.iCodAreaGen,
		AGEN.vDescripcion AS AreaGeneracion,
		R.iCodGerenteGen,
		GERGEN.vNombres AS GerenteGeneracion,
		R.iCodResponsableGen1,
		RESP1GEN.vNombres AS ResponsableGeneracion1,
		R.iCodResponsableGen2,
		RESP2GEN.vNombres AS ResponsableGeneracion2,
		R.iCodAreaResponsable,
		ARES.vDescripcion AS AreaResponsable,
		R.iCodGerenteResponsable,
		GERRESP.vNombres AS GerenteResponsable,
		R.iCodCoordinador,
		COORESP.vNombres AS Coordinador,
		R.iCodResponsablePrincipal,
		RESPP.vNombres AS ResponsablePrincipal,
		R.iCodResponsableAlterno,
		RESPA.vNombres AS ResponsableAlterno,
		R.iCodFrecuencia,
		PARF.vDescripcion AS Frecuencia,
		R.dProximoVencimiento,
		R.iCodTipoCalculo,
		R.iPlazo,
		R.iTipoDiasPlazo,
		R.iDiasRecordatorio1,
		R.iDiasRecordatorio2,
		R.iEstado		
	FROM MaeReporte R
		INNER JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		INNER JOIN MaeArea AGEN ON R.iCodAreaGen = AGEN.iCodArea
		INNER JOIN MaeUsuario GERGEN ON R.iCodGerenteGen = GERGEN.iCodUsuario
		INNER JOIN MaeUsuario RESP1GEN ON R.iCodResponsableGen1 = RESP1GEN.iCodUsuario
		INNER JOIN MaeUsuario RESP2GEN ON R.iCodResponsableGen2 = RESP2GEN.iCodUsuario
		INNER JOIN MaeArea ARES ON R.iCodAreaResponsable = ARES.iCodArea
		INNER JOIN MaeUsuario GERRESP ON R.iCodGerenteResponsable = GERRESP.iCodUsuario
		INNER JOIN MaeUsuario COORESP ON R.iCodCoordinador = COORESP.iCodUsuario
		INNER JOIN MaeUsuario RESPP ON R.iCodResponsablePrincipal = RESPP.iCodUsuario
		LEFT JOIN MaeUsuario RESPA ON R.iCodResponsableAlterno = RESPA.iCodUsuario
		LEFT JOIN ParParametro PARME ON (PARME.iCodGrupo = 2 AND R.iMedioEnvio = PARME.iCodElemento)
		LEFT JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
	WHERE (@VctoDesde IS NULL OR (@VctoDesde IS NOT NULL AND CONVERT(DATE, R.dProximoVencimiento) >= CONVERT(DATE, @VctoDesde)))
		AND (@VctoHasta IS NULL OR (@VctoHasta IS NOT NULL AND CONVERT(DATE, R.dProximoVencimiento) <= CONVERT(DATE, @VctoHasta)))
		AND (@Areas = '0' OR (LEN(@Areas) > 0 AND @Areas <> '0' AND R.iCodAreaResponsable IN (SELECT * FROM dbo.Split(@Areas, ','))))
	ORDER BY CAST(R.dProximoVencimiento AS DATE) DESC, R.vNombre

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaSeguimiento]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaSeguimiento]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaSeguimiento]
(	
	@Areas VARCHAR(50),	
	@VctoDesde DATETIME NULL,
	@VctoHasta DATETIME NULL,
	@EstadoCumplimiento INT NULL
)
AS
BEGIN
	
	SELECT
		R.iCodReporte,
		R.vNombre,
		R.vDescripcion,
		R.iCodEmpresa,
		E.vDescripcion AS Empresa,
		R.iMedioEnvio,
		PARME.vDescripcion AS MedioEnvio,
		R.iCodAreaGen,
		AGEN.vDescripcion AS AreaGeneracion,
		R.iCodGerenteGen,
		GERGEN.vNombres AS GerenteGeneracion,
		R.iCodResponsableGen1,
		RESP1GEN.vNombres AS ResponsableGeneracion1,
		R.iCodResponsableGen2,
		RESP2GEN.vNombres AS ResponsableGeneracion2,
		R.iCodAreaResponsable,
		ARES.vDescripcion AS AreaResponsable,
		R.iCodGerenteResponsable,
		GERRESP.vNombres AS GerenteResponsable,
		R.iCodCoordinador,
		COORESP.vNombres AS Coordinador,
		R.iCodResponsablePrincipal,
		RESPP.vNombres AS ResponsablePrincipal,		
		R.iCodResponsableAlterno,
		RESPA.vNombres AS ResponsableAlterno,
		R.iCodFrecuencia,
		PARF.vDescripcion AS Frecuencia,
		R.dProximoVencimiento,
		R.iCodTipoCalculo,
		R.iPlazo,
		R.iTipoDiasPlazo,
		R.iDiasRecordatorio1,
		R.iDiasRecordatorio2,
		R.iEstado,
		EN.iCodEnvio,
		(CASE 
			WHEN EN.iCodReporte IS NULL THEN 4
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NULL THEN 3
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) > CONVERT(DATE, EN.dFecVencimiento) THEN 2
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) <= CONVERT(DATE, EN.dFecVencimiento) THEN 1
		END) AS Cumplimiento,
		(CASE 
			WHEN EN.iCodReporte IS NULL THEN 'NO INFORMADO'
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NULL THEN 'NO ENVIADO'
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) > CONVERT(DATE, EN.dFecVencimiento) THEN 'FUERA DEL PLAZO'
			WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) <= CONVERT(DATE, EN.dFecVencimiento) THEN 'DENTRO DEL PLAZO'
		END) AS DescripcionCumplimiento,
		EN.vNombreReporte AS NombreReporteE,
		EN.vResponsablePrincipal AS ResponsablePrincipalE,
		EN.dFecRegistro AS FechaRegistro,
		EN.dFecVencimiento AS FechaVencimiento,
		(CASE WHEN EN.iCodReporte IS NOT NULL THEN MONTH(EN.dFecVencimiento) ELSE MONTH(R.dProximoVencimiento) END) AS MesVencimiento,
		(CASE WHEN EN.iCodReporte IS NOT NULL THEN YEAR(EN.dFecVencimiento) ELSE YEAR(R.dProximoVencimiento) END) AS AnioVencimiento,		
		EN.dFecEnvio AS FechaEnvio,
		EN.vMotivoIncumplimiento,
		EN.vComentarios
	FROM MaeReporte R
		INNER JOIN MaeReporteVencimiento RV ON R.iCodReporte = RV.iCodReporte AND RV.iEstado = 1
		INNER JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		INNER JOIN MaeArea AGEN ON R.iCodAreaGen = AGEN.iCodArea
		INNER JOIN MaeUsuario GERGEN ON R.iCodGerenteGen = GERGEN.iCodUsuario
		INNER JOIN MaeUsuario RESP1GEN ON R.iCodResponsableGen1 = RESP1GEN.iCodUsuario
		INNER JOIN MaeUsuario RESP2GEN ON R.iCodResponsableGen2 = RESP2GEN.iCodUsuario
		INNER JOIN MaeArea ARES ON R.iCodAreaResponsable = ARES.iCodArea
		INNER JOIN MaeUsuario GERRESP ON R.iCodGerenteResponsable = GERRESP.iCodUsuario
		INNER JOIN MaeUsuario COORESP ON R.iCodCoordinador = COORESP.iCodUsuario
		INNER JOIN MaeUsuario RESPP ON R.iCodResponsablePrincipal = RESPP.iCodUsuario
		LEFT JOIN MaeUsuario RESPA ON R.iCodResponsableAlterno = RESPA.iCodUsuario
		LEFT JOIN ParParametro PARME ON (PARME.iCodGrupo = 2 AND R.iMedioEnvio = PARME.iCodElemento)
		LEFT JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
		LEFT JOIN MovEnvio EN ON (R.iCodReporte = EN.iCodReporte AND RV.iCodVencimiento = EN.iCodVencimiento)
	WHERE (@VctoDesde IS NULL OR (@VctoDesde IS NOT NULL AND CONVERT(DATE, R.dProximoVencimiento) >= CONVERT(DATE, @VctoDesde)))
		AND (@VctoHasta IS NULL OR (@VctoHasta IS NOT NULL AND CONVERT(DATE, R.dProximoVencimiento) <= CONVERT(DATE, @VctoHasta)))
		AND (@Areas = '0' OR (LEN(@Areas) > 0 AND @Areas <> '0' AND R.iCodAreaResponsable IN (SELECT * FROM dbo.Split(@Areas, ','))))
		AND (@EstadoCumplimiento IS NULL 
			OR (@EstadoCumplimiento IS NOT NULL AND 
			@EstadoCumplimiento = 
			(CASE 
				WHEN EN.iCodReporte IS NULL THEN 4
				WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NULL THEN 3
				WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) > CONVERT(DATE, EN.dFecVencimiento) THEN 2
				WHEN EN.iCodReporte IS NOT NULL AND EN.dFecEnvio IS NOT NULL AND CONVERT(DATE, EN.dFecEnvio) <= CONVERT(DATE, EN.dFecVencimiento) THEN 1
			END)
			))
	ORDER BY EN.dFecVencimiento, EN.vNombreReporte, R.dProximoVencimiento DESC, R.vNombre

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RegistrarReporte]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_RegistrarReporte]
GO

CREATE PROCEDURE [dbo].[USP_RegistrarReporte]
(
	@CodReporte INT NULL,	
	@Nombre VARCHAR(50),
	@Descripcion VARCHAR(300),
	@CodEmpresa INT,
	@MedioEnvio INT NULL,
	@CodAreaGen INT,
	@CodGerenteGen INT,
	@CodResponsableGen1 INT,
	@CodResponsableGen2 INT,
	@CodAreaResponsable INT,
	@CodGerenteResponsable INT,
	@CodCoordinador INT,
	@CodResponsablePrincipal INT,
	@CodResponsableAlterno INT NULL,
	@CodFrecuencia INT,
	@ProximoVencimiento DATETIME,
	@CodTipoCalculo INT,
	@Plazo INT,
	@TipoDiasPlazo INT,
	@DiasRecordatorio1 INT,
	@DiasRecordatorio2 INT,
	@Estado INT,
	@UsuCreacion VARCHAR(100),
	@UsuActualizacion VARCHAR(100) NULL
)
AS
BEGIN
	
	DECLARE @NombreGenerado VARCHAR(50) = '';

	IF(@Nombre IS NULL OR @Nombre = '' OR LEN(@Nombre) = 0 )
	BEGIN
		
		DECLARE @MAXID INT = (SELECT MAX(iCodReporte) FROM MaeReporte)
		DECLARE @NomReporte VARCHAR(50) =(SELECT CONCAT('REP', FORMAT((@MAXID + 1), '00#')))
		SET @NombreGenerado = @NomReporte;
	END

	IF(@Nombre IS NOT NULL OR LEN(@Nombre) > 0)
	BEGIN
		
		SET @NombreGenerado = @Nombre
	END

	
	IF (@CodReporte IS NULL OR @CodReporte = 0 )
	BEGIN

		INSERT INTO MaeReporte 
		(vNombre, vDescripcion, iCodEmpresa, iMedioEnvio, iCodAreaGen, iCodGerenteGen, iCodResponsableGen1, iCodResponsableGen2, 
		iCodAreaResponsable, iCodGerenteResponsable, iCodCoordinador, iCodResponsablePrincipal, iCodResponsableAlterno, iCodFrecuencia, 
		dProximoVencimiento, iCodTipoCalculo, iPlazo, iTipoDiasPlazo, iDiasRecordatorio1, iDiasRecordatorio2, iEstado, vUsuCreacion, 
		dFecCreacion)
		VALUES
		(@NombreGenerado, @Descripcion, @CodEmpresa, @MedioEnvio, @CodAreaGen, @CodGerenteGen, @CodResponsableGen1, @CodResponsableGen2, 
		@CodAreaResponsable, @CodGerenteResponsable, @CodCoordinador, @CodResponsablePrincipal, @CodResponsableAlterno, @CodFrecuencia, 
		@ProximoVencimiento, @CodTipoCalculo, @Plazo, @TipoDiasPlazo, @DiasRecordatorio1, @DiasRecordatorio2, @Estado, @UsuCreacion,
		GETDATE())
		
		DECLARE @iCodReporte INT = @@IDENTITY

		INSERT INTO MaeReporteVencimiento(iCodReporte, iEstadoVencimiento, iRecordatorio1, iRecordatorio2, iAlertaVencimiento, iAlertaIncumplimiento, iEstado, vUsuCreacion, dFecCreacion)
		VALUES (@iCodReporte, 1, 1, 1, 1, 1, 1, @UsuCreacion, GETDATE())
				
	END
	ELSE
	BEGIN
		
		UPDATE MaeReporte
		SET	vNombre = @NombreGenerado,
			vDescripcion = @Descripcion,
			iCodEmpresa = @CodEmpresa,			
			iMedioEnvio = @MedioEnvio,
			iCodAreaGen = @CodAreaGen,
			iCodGerenteGen = @CodGerenteGen,
			iCodResponsableGen1 = @CodResponsableGen1,
			iCodResponsableGen2 = @CodResponsableGen2 ,
			iCodAreaResponsable = @CodAreaResponsable,
			iCodGerenteResponsable = @CodGerenteResponsable,
			iCodCoordinador = @CodCoordinador,		
			iCodResponsablePrincipal = @CodResponsablePrincipal,
			iCodResponsableAlterno = @CodResponsableAlterno,
			iCodFrecuencia = @CodFrecuencia,
			dProximoVencimiento = @ProximoVencimiento,
			iCodTipoCalculo = @CodTipoCalculo,
			iPlazo = @Plazo,
			iTipoDiasPlazo = @TipoDiasPlazo ,
			iDiasRecordatorio1 = @DiasRecordatorio1,
			iDiasRecordatorio2 = @DiasRecordatorio2,
			iEstado = @Estado,
			vUsuActualizacion = @UsuActualizacion,
			dFecActualizacion = GETDATE()
		WHERE iCodReporte = @CodReporte
	END

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaParametro]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaParametro]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaParametro]
(
	@CodGrupo INT
)
AS
BEGIN
	
	SELECT [iCodParametro]
		,[iCodGrupo]
		,[iCodElemento]
		,[vDescripcion]
		,[vValor]
		,[iOrden]
		,[iEstado]		
	FROM ParParametro
	WHERE iEstado = 1
		AND iCodElemento <> 0
		AND iCodGrupo = @CodGrupo

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaArea]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaArea]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaArea]
AS
BEGIN
	
	SELECT [iCodArea]
      ,[vDescripcion]
      ,[iEstado]      
	FROM [DBAgrCumplimiento].[dbo].[MaeArea]
	WHERE iEstado = 1

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaEmpresa]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaEmpresa]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaEmpresa]
AS
BEGIN
	
	SELECT [iCodEmpresa]
      ,[vDescripcion]
      ,[iEstado]      
	FROM [DBAgrCumplimiento].[dbo].[MaeEmpresa]
	WHERE iEstado = 1

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaUsuario]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaUsuario]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaUsuario]
AS
BEGIN
	
	SELECT [iCodUsuario]
      ,[vNombres]
      ,[vEmail]
      ,[vWEBUSER]
      ,[vCargo]
      ,[iCodArea]
      ,[iEstado]
	FROM [DBAgrCumplimiento].[dbo].[MaeUsuario]
	WHERE iEstado = 1

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ValidarNombreReporte]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ValidarNombreReporte]
GO

CREATE PROCEDURE [dbo].[USP_ValidarNombreReporte]
(
	@NombreReporte VARCHAR(50)	
)
AS
BEGIN
	
	SELECT 
		iCodReporte,
		vNombre
	FROM MaeReporte R
	WHERE vNombre = RTRIM(LTRIM(@NombreReporte))
		AND iEstado = 1

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaEnvioBusqueda]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaEnvioBusqueda]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaEnvioBusqueda]
(	
	@Entidades VARCHAR(50),
	@Frecuencias VARCHAR(50),
	@VctoDesde DATETIME NULL,
	@VctoHasta DATETIME NULL
)
AS
BEGIN
	
	SELECT
		iCodEnvio,
		E.iCodReporte AS CodReporte,
		E.vNombreReporte AS NombreReporte,
		vEmpresa,
		vAreaResponsable,
		vGerenteResponsable,
		vResponsablePrincipal,
		vResponsableAlterno,
		iEstadoEnvio,
		E.iCodVencimiento,
		dFecVencimiento,
		dFecEnvio,
		dFecRegistro,
		vMotivoIncumplimiento,
		vComentarios,
		iCodSustento,
		vNombreArchivo,
		PARF.vDescripcion AS Frecuencia,
		E.iCodResponsablePrincipal,
		E.iCodResponsableAlterno,
		R.iCodGerenteResponsable,
		R.iCodCoordinador
	FROM MovEnvio E		
		INNER JOIN MaeReporteVencimiento RV ON E.iCodReporte = RV.iCodReporte AND E.iCodVencimiento = RV.iCodVencimiento AND RV.iEstado = 1
		INNER JOIN MaeReporte R ON RV.iCodReporte = R.iCodReporte
		INNER JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
	WHERE (@Entidades = '0' OR (LEN(@Entidades) > 0 AND @Entidades <> '0' AND R.iCodEmpresa IN (SELECT * FROM dbo.Split(@Entidades, ','))))
		AND (@Frecuencias = '0' OR (LEN(@Frecuencias) > 0 AND @Frecuencias <> '0' AND R.iCodFrecuencia IN (SELECT * FROM dbo.Split(@Frecuencias, ','))))
		AND (@VctoDesde IS NULL OR (@VctoDesde IS NOT NULL AND CONVERT(DATE, E.dFecVencimiento) >= CONVERT(DATE, @VctoDesde)))
		AND (@VctoHasta IS NULL OR (@VctoHasta IS NOT NULL AND CONVERT(DATE, E.dFecVencimiento) <= CONVERT(DATE, @VctoHasta)))

END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_RegistrarEnvio]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_RegistrarEnvio]
GO

CREATE PROCEDURE [dbo].[USP_RegistrarEnvio]
(	
	@CodReporte INT,
	@NombreReporte VARCHAR(50) NULL,	
	@Entidad VARCHAR(300) NULL,
	@AreaResponsable VARCHAR(200) NULL,
	@GerenteResponsable VARCHAR(400) NULL,
	@CodResponsablePrincipal INT,
	@ResponsablePrincipal VARCHAR(400) NULL,
	@CodResponsableAlterno INT,
	@ResponsableAlterno VARCHAR(400) NULL,
	@EstadoEnvio INT,	
	@FechaVencimiento DATETIME,
	@FechaEnvio DATETIME NULL,	
	@MotivoIncumplimiento VARCHAR(200) NULL,
	@Comentarios VARCHAR(500) NULL,
	@CodSustento INT,
	@NombreArchivo VARCHAR(100) NULL,
	@UsuCreacion VARCHAR(100)
)
AS
BEGIN
	
	DECLARE @CodVencimiento INT
	
	SELECT @CodVencimiento =  iCodVencimiento
	FROM MaeReporteVencimiento 
	WHERE iCodReporte = @CodReporte
		AND iEstado = 1
		AND iEstadoVencimiento = 1
	
	INSERT INTO MovEnvio
	(iCodReporte, vNombreReporte, vEmpresa, vAreaResponsable, vGerenteResponsable, iCodResponsablePrincipal, vResponsablePrincipal, iCodResponsableAlterno, vResponsableAlterno,
	iEstadoEnvio, iCodVencimiento, dFecVencimiento, dFecEnvio, dFecRegistro, vMotivoIncumplimiento, vComentarios, iCodSustento,
	vNombreArchivo, vUsuCreacion, dFecCreacion)
	VALUES
	(@CodReporte, @NombreReporte, @Entidad, @AreaResponsable, @GerenteResponsable, @CodResponsablePrincipal, @ResponsablePrincipal, @CodResponsableAlterno, @ResponsableAlterno,
	@EstadoEnvio, @CodVencimiento, @FechaVencimiento, @FechaEnvio, GETDATE(), @MotivoIncumplimiento, @Comentarios, @CodSustento,
	@NombreArchivo, @UsuCreacion, GETDATE())

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ActualizarFechaVencimientoReporte]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ActualizarFechaVencimientoReporte]
GO

CREATE PROCEDURE [dbo].[USP_ActualizarFechaVencimientoReporte]
(	
	@CodReporte INT,	
	--@CodVencimiento INT NULL,
	@FechaVencimiento DATETIME,
	@UsuActualizacion VARCHAR(100)
)
AS
BEGIN
	
	/*ACTUALIZAMOS LA FECHA DE VENCIMIENTO DEL REPORTE*/
	/*OBTENEMOS EN BASE A UN C�LCULO RESPECTIVO O LO RECIBIMOS POR PAR�METRO*/
	--DECLARE @NFechaVencimiento DATETIME = GETDATE()

	UPDATE MaeReporte
	SET dProximoVencimiento = @FechaVencimiento,
		vUsuActualizacion = @UsuActualizacion,
		dFecActualizacion = GETDATE()
	WHERE iCodReporte = @CodReporte

	
	/*DESACTIVAMOS LA FECHA DE VENCIMIENTO ACTUAL*/
	UPDATE MaeReporteVencimiento
	SET iEstadoVencimiento = 0,
		vUsuActualizacion = @UsuActualizacion,
		dFecActualizacion = GETDATE()
	WHERE iCodReporte = @CodReporte
		--AND iCodVencimiento = @CodVencimiento
		AND iEstadoVencimiento = 1
		AND iEstado = 1

	/*GENERAMOS NUEVO CODVENCIMIENTO*/
	INSERT INTO MaeReporteVencimiento(iCodReporte, iEstadoVencimiento, iRecordatorio1, iRecordatorio2, iAlertaVencimiento, 
			iAlertaIncumplimiento, iEstado, vUsuCreacion, dFecCreacion)
	VALUES (@CodReporte, 1, 0, 0, 0, 0, 1, @UsuActualizacion, GETDATE())

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ObtenerListaEnvio]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ObtenerListaEnvio]
GO

CREATE PROCEDURE [dbo].[USP_ObtenerListaEnvio]
(	
	@CodEnvio INT NULL
)
AS
BEGIN
	
	SELECT
		E.iCodEnvio,
		E.iCodReporte AS CodReporte,
		E.vNombreReporte AS NombreReporte,
		R.iCodEmpresa AS CodEntidad,
		R.iCodAreaResponsable AS CodAreaResponsable,
		R.iCodGerenteResponsable AS CodGerenteResponsable,
		E.iCodResponsablePrincipal AS CodResponsablePrincipal,		
		E.iCodResponsableAlterno AS CodResponsableAlterno,
		E.dFecVencimiento AS FechaVencimiento,
		E.iEstadoEnvio AS EstadoEnvio,
		E.dFecEnvio AS FechaEnvio,
		E.vMotivoIncumplimiento,
		E.vComentarios,
		E.iCodSustento,
		E.vNombreArchivo,
		PARF.vDescripcion AS Frecuencia,
		E.vEmpresa,
		E.vAreaResponsable,
		E.vGerenteResponsable,
		E.vResponsablePrincipal,
		E.vResponsableAlterno,		
		E.iCodVencimiento
	FROM MovEnvio E		
		INNER JOIN MaeReporteVencimiento RV ON E.iCodReporte = RV.iCodReporte AND E.iCodVencimiento = RV.iCodVencimiento AND RV.iEstado = 1
		INNER JOIN MaeReporte R ON RV.iCodReporte = R.iCodReporte
		INNER JOIN ParParametro PARF ON (PARF.iCodGrupo = 1 AND R.iCodFrecuencia = PARF.iCodElemento)
	WHERE (@CodEnvio IS NULL OR (@CodEnvio IS NOT NULL AND E.iCodEnvio = @CodEnvio))	

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_ActualizarReporteNotificacion]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[USP_ActualizarReporteNotificacion]
GO

CREATE PROCEDURE [dbo].[USP_ActualizarReporteNotificacion]
(	
	@CodReporte INT,
	@TipoNotificacion INT
)
AS
BEGIN	
		
	UPDATE MaeReporteVencimiento
	SET	iRecordatorio1 = (CASE @TipoNotificacion WHEN 1 THEN 1 ELSE iRecordatorio1 END),
		iRecordatorio2 = (CASE @TipoNotificacion WHEN 2 THEN 1 ELSE iRecordatorio2 END),
		iAlertaVencimiento = (CASE @TipoNotificacion WHEN 3 THEN 1 ELSE iAlertaVencimiento END),
		iAlertaIncumplimiento = (CASE @TipoNotificacion WHEN 4 THEN 1 ELSE iAlertaIncumplimiento END),
		vUsuActualizacion = 'NOTIFICACIONES',
		dFecActualizacion = GETDATE()
	WHERE iCodReporte = @CodReporte		
		AND iEstadoVencimiento = 1
		AND iEstado = 1
		
END
GO